<!-- page -->

<div class="container perso-container">

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $controller->alertVentes; ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  	<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<h1 class="center">Formulaire d'enregistrement des objectifs ou des ventes de journaux</h1><br>

	<div class="row">
		<div class="col-md-2"/></div>
		<div class="col-md-8 perso-form-1">
			<h4 class="center bold">Nouvelle vente ou objectif</h4><br>
			<form method="post" action="#">
				<fieldset>
					<legend class="center bold">Statut</legend>
					<p class="center">
						Il s'agit d'un enregistrement de ventes réelles ou d'objectifs?<br/>
					</p>
					<div class="bloc-center">
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="statut" value="vente" id="vente" />
							<label class="form-check-label" for="vente">Ventes</label><br />
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="statut" value="objectif" id="objectif" />
							<label class="form-check-label" for="objectif">Objectifs</label><br/>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend class="center bold">Période</legend>
					<div class="form-group row center">
						<label class="col-sm-12 col-form-label center" for="an">Année</label>
						<div class="col-sm-12 bloc-center">
							<select class="custom-select col-sm-4 bloc-center"  id="an" name="an">
								<option selected></option>
								<option value="2018">2018</option>
								<option value="2019">2019</option>
								<option value="2020">2020</option>
								<option value="2021">2021</option>
								<option value="2022">2022</option>
								<option value="2023">2023</option>
								<option value="2024">2024</option>
								<option value="2025">2025</option>
								<option value="2026">2026</option>
								<option value="2027">2026</option>
								<option value="2028">2026</option>
								<option value="2029">2026</option>
								<option value="2030">2026</option>
								<option value="2026">2031</option>
							</select><br>
						</div>
					</div>
					<div class="form-group row center">
						<label class="col-sm-12 col-form-label center" for="mois">Mois</label>
						<div class="col-sm-12 bloc-center">
							<select class="custom-select col-sm-4 bloc-center" id="mois" name="mois">
								<option value="0"></option>
								<option value="1">Janvier Février</option>
								<option value="2">Mars Avril</option>
								<option value="3">Mai Juin</option>
								<option value="4">Juillet Août</option>
								<option value="5">Septembre Octobre</option>
								<option value="6">Novembre Décembre</option>
							</select><br>
						</div>
					</div>
				</fieldset>
				<fieldset class="vente">
					<legend class="center bold">Ventes réelles</legend>
					<div class="form-group row center">
						<label class="col-sm-4 col-form-label" for="nombre">Nombre vendus</label>
						<div class="col-sm-6">
							<input class="form-control" type="number" name="nombre" id="nombre" min="0" max="1000" placeholder="0"><br>
						</div>
					</div>
					<div class="form-group row center">
						<label class="col-sm-4 col-form-label" for="montant">Montant</label>
						<div class="col-sm-6">
							<input class="form-control" type="number" name="montant" id="montant" min="0" max="1000" placeholder="0,00 (firefox) 0.00 (chrome)" step="0.01"> €
						</div>
					</div>	
				</fieldset>
				<fieldset class="objectif">
					<legend class="center bold">Objectifs</legend>
					<div class="form-group row center">
						<label class="col-sm-4 col-form-label" for="objectif_nombre">Nombre à vendre</label>
						<div class="col-sm-6">
							<input class="form-control" type="number" name="objectif_nombre" id="objectif_nombre" min="0" max="1000" placeholder="0"><br>
						</div>
					</div>
					<div class="form-group row center">
						<label class="col-sm-4 col-form-label" for="objectif_montant">Montant à atteindre</label>
						<div class="col-sm-6">
							<input class="form-control" type="number" name="objectif_montant" id="objectif_montant" min="0" max="5000" placeholder="0,00 (firefox) 0.00 (chrome)" step="0.01"> €
						</div>
					</div>
				</fieldset>
				<input class="btn btn-primary bloc-center" type="submit" name="send_vente_journal" value="Enregistrer"><br>
				<div class="col-md-12">
					<a href="index.php?p=Journaux"><input type="submit" class="btn btn-dark bloc-center" value="Retour" /></a><br>
				</div>
			</form>
		</div>
	</div>
</div>
