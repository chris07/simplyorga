<div class="container perso-container">

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $controller->alertAjoutCat; ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true">&times;</span>
	  	</button>
	</div>

	<h1 class="center">Formulaire d'ajout d'une Catégorie</h1><br>

	<div class="row">
		<div class="col-md-3"/></div>
		<div class="col-md-6 perso-form-1">
			<h4 class="center bold">Nouvelle Catégorie</h4><br>
			<form method="post" action="#">
				<div class="form-group row center">
					<label class="col-sm-2 col-form-label" for="nom">Nom :</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="nom" id="nom" value="<?php if(isset($_POST['nom'])){echo htmlspecialchars($_POST['nom']);} ?>">
					</div>
				</div>
				<fieldset>
					<legend class="center bold">Catégorie parente</legend>
					<div class="bloc-center">
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" id="materiel" name="parent_cat"  value="materiel">
							<label class="form-check-label" for="materiel">Matériel</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" id="evenementiel" name="parent_cat"  value="evenementiel">
							<label class="form-check-label" for="evenementiel">Évènementiel</label>
						</div>
					</div>
				</fieldset>
				<br>
				<div>
					<input class="btn btn-primary bloc-center" type="submit" name="new_cat" value="Enregistrer">
				</div>
			</form><br>
			<div class="col-sm-12">
				<form method="post" action="#">
					<input type="submit" name="liste_categories" class="btn btn-dark bloc-center" value="Retour" /><br>
				</form>
			</div>	
		</div>	
		<div class="col-md-3"/></div>
	</div>

</div>