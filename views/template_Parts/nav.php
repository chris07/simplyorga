<?php

require_once  ROOT_PATH .'/app/membersArea.php';

?>
 <div class="collapse navbar-collapse" id="navbarNavDropdown">
<ul class= "navbar-nav">
    <li class="nav-item"><img src="https://simplyorga.sky-zoo.fr/img/logo.png" class="logo" alt="logo"/></li>
    <li class= "nav-item"><a class="nav-link" href="index.php">Accueil</a></li>
    <?php
        foreach($page as $key => $value){
            if( is_connected() ) {
                if( $value['visibility'] == 'private' && $value['menu'] != false ){
                    echo '<li class="nav-item"><a class= "nav-link" href="index.php?p='. $key . '">' . ucfirst($value['title']) . '</a></li>';
                }
            }
            else {
                if( $value['visibility'] == 'public' && $value['menu'] != false ){
                    echo '<li class="nav-item"><a class="nav-link" href="index.php?p='. $key . '">' . ucfirst($value['title']) . '</a></li>';
                }         
            }    
        }
    ?>
</ul>
</div>

