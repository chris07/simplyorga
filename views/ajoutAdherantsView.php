<div class="container perso-container">


	<h1 class="center">Formulaire d'ajout d'un nouvel Adhérant</h1><br>

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $alertAjoutAdh; ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div class="row">
		<div class="col-md-2"/></div>
		<div class="col-md-8 perso-form-1">
			<h4 class="center bold">Nouvel Adhérant</h4><br>
			<form method="post" action="#">
				<fieldset>
					<legend class="center bold">Coordonnées</legend>
					<div class="form-group row center">
						<label class="col-sm-3 col-form-label" for="nom">Nom :</label>
						<div class="col-sm-7">
							<input class="form-control" type="text" name="nom" id="nom">
						</div>
					</div>
					<div class="form-group row center">
						<label class="col-sm-3 col-form-label" for="prenom">Prénom :</label>
						<div class="col-sm-7">
							<input class="form-control" type="text" name="prenom" id="prenom">
						</div>
					</div>
					<div class="form-group row center">
						<label class="col-sm-3 col-form-label" for="adresse">Adresse :</label>
						<div class="col-sm-7">
							<input class="form-control" type="text" name="adresse" id="adresse">
						</div>
					</div>
					<div class="form-group row center">
						<label class="col-sm-3 col-form-label" for="tel">Téléphone :</label>
						<div class="col-sm-7">
							<input class="form-control" type="tel" name="tel" id="tel">
						</div>
					</div>
					<div class="form-group row center">
						<label class="col-sm-3 col-form-label" for="mail">e-mail :</label>
						<div class="col-sm-7">
							<input class="form-control" type="email" name="mail" id="mail">
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend class="center bold">Abonnement au journal</legend>
					<p class="center">
						Cet adhérant est-il abonné au journal?<br/>
					</p>
					<div class="bloc-center">
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="abonnement" value="oui" id="abonne" />
							<label class="form-check-label" for="abonne">Oui</label><br />
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="abonnement" value="non" id="non_abonne" />
							<label class="form-check-label" for="non_abonne">Non</label><br />
						</div>
					</div>
					<div class="date form-group row center">
						<label class="col-sm-4 col-form-label" for="date">Date d'abonnement :</label>
						<div class="col-sm-4 bloc-center">
							<input class="form-control" type="date" name="date" id="date">
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend class="center bold">Cotisation</legend>
					<p class="center">
						Cet adhérant cotise t'il?<br/>
					</p>
					<div class="bloc-center">
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="cotisation" value="oui" id="cotise" />
							<label class="form-check-label" for="cotise">Oui</label><br />
							<input class="form-check-input" type="radio" name="cotisation" value="non" id="non_cotise" />
							<label class="form-check-label" for="non_cotise">Non</label><br />
						</div>
					</div>
					<div class="coti form-group row center">
						<h4 class="center bold">Paiement Cotisation</h4>
						<label class="col-sm-4 col-form-label" for="coti">Mode de paiement de la cotisation ?</label><br/>
						<div class="col-sm-12 bloc-center">
							<select  class="custom-select col-sm-4 bloc-center" name="coti" id="coti">
								<option value=""></option>
								<option value="espece">Espèce</option>
								<option value="cheque">Chèque</option>
								<option value="virement">Virement Bancaire</option>
							</select>
						</div>	
					</div>
				</fieldset>
				<input class="btn btn-primary bloc-center" type="submit" name="new_adh" value="Enregistrer">
			</form><br>
			<div class="col-md-12">
				<a href="index.php?p=liste_adh"><input type="submit" class="btn btn-dark bloc-center" value="Retour" /></a><br>
			</div>	
		</div>
		<div class="col-md-2"/></div>
	</div>
</div>