<div class="perso-container-2">
	
	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?=  $alertdetailcoti ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		   	<span aria-hidden="true">&times;</span>
		</button>
	</div>


	<h1 class="center">Cotisations de <?= $adh_infos[0]['nom']. " " . $adh_infos[0]['prenom'] ?> Pour l'année <?= $year ?></h1><br>

	<div class="row">
		<div class="col-md-12 perso-form-1">
			<div class="col-sm-12">
				<form method="post" action="index.php?p=saisie_cotisations">
					<input class="btn btn-dark bloc-center" type="submit" name="saisie_cotisations" value="Retour"/><br>
				</form>
			</div>	
			<form method="post" action="#">
				<div class="table-responsive">
					<table class="table" id='tab-coti'>
						<tr>
							<td>Janvier</td>
							<td>Février</td>
							<td>Mars</td>
							<td>Avril</td>
							<td>Mai</td>
							<td>Juin</td>
							<td>Juillet</td>
							<td>Août</td>
							<td>Septembre</td>
							<td>Octobre</td>
							<td>Novembre</td>
							<td>Décembre</td>
							<td></td>
						</tr>
						<tr>
							<td><input class="input form-control" type="number" name="0" step="0.01" value="<?php echo $janvier ?>"></td>
							<td><input class="input form-control" type="number" name="1" step="0.01" value="<?php echo $fevrier ?>"></td>
							<td><input class="input form-control" type="number" name="2" step="0.01" value="<?php echo $mars ?>"></td>
							<td><input class="input form-control" type="number" name="3" step="0.01" value="<?php echo $avril ?>"></td>
							<td><input class="input form-control" type="number" name="4" step="0.01" value="<?php echo $mai ?>"></td>
							<td><input class="input form-control" type="number" name="5" step="0.01" value="<?php echo $juin ?>"></td>
							<td><input class="input form-control" type="number" name="6" step="0.01" value="<?php echo $juillet ?>"></td>
							<td><input class="input form-control" type="number" name="7" step="0.01" value="<?php echo $aout ?>"></td>
							<td><input class="input form-control" type="number" name="8" step="0.01" value="<?php echo $septembre ?>"></td>
							<td><input class="input form-control" type="number" name="9" step="0.01" value="<?php echo $octobre ?>"></td>
							<td><input class="input form-control" type="number" name="10" step="0.01" value="<?php echo $novembre ?>"></td>
							<td><input class="input form-control" type="number" name="11" step="0.01" value="<?php echo $decembre ?>"></td>
							<td><input class="btn btn-success" type="submit" name="submit_details_coti" id="submit" value="Enregistrer"></td>
						</tr>
						
					</table>
					<input type="hidden" name="year" value="<?php echo $year ?>">
					<input type="hidden" name="id_adh" value="<?php echo $adh_infos[0]['id'] ?>">
				</div>
			</form>
		</div>
	</div>
</div>
