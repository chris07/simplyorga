<!-- Modal -->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="confirm-update">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <h4 class="modal-title" id="myModalLabel">Voulez-vous vraiement modifier cette Catégorie</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="btn-no">Non</button>
        <button type="button" class="btn btn-primary" id="btn-yes">Oui</button>
      </div>
    </div>
  </div>
</div>

<!-- Page -->

<div class="container perso-container">

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $controller->alertModifSuppCategorie; ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true">&times;</span>
	  	</button>
	</div>

	<h1 class="center">Formulaire de modification d'une Catégorie</h1><br>

	<div class="row">
		<div class="col-md-3"/></div>
		<div class="col-md-6 perso-form-1">
			<h4 class="center bold">Catégorie</h4><br>
			<form method="post" action="#">
				<div class="form-group row center">
					<label class="col-sm-2 col-form-label" for="nom">Nom :</label>
					<div class="col-sm-8">
						<input class="form-control"type="text" name="nom" id="nom" value="<?=  $controller->data[0]['nom']; ?>">
					</div>
				</div>
				<fieldset>
					<legend class="center bold">Catégorie parente</legend>
					<div class="bloc-center">
						<div class="form-check form-check-inline">
							<input type="radio" id="materiel" name="parent_cat"  value="materiel" <?php if($controller->data[0]['id_cat'] === '1'){echo 'checked' ;} ?>>
							<label class="form-check-label" for="materiel">Matériel</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" id="evenementiel" name="parent_cat"  value="evenementiel" 
							<?php if($controller->data[0]['id_cat'] === '2'){echo 'checked' ;} ?>>
							<label class="form-check-label" for="evenementiel">Évènementiel</label>
						</div>
					</div>
				</fieldset>	
				<br>	
				<input type="hidden" name="id" value="<?php echo $controller->data[0]['id'] ; ?>">
				<input type="hidden" name="bouton_modif_cat" value="bouton_modifier">
				<div class="form-group row center">
					<input class="btn btn-primary bloc-center" type="submit" name="update_cat" value="Enregistrer">
				</div>
			</form><br>
			<div class="col-sm-12">
				<form method="post" action="#">
					<input type="submit" name="liste_categories" class="btn btn-dark bloc-center" value="Retour" /><br>
				</form>
			</div>	
		</div>
		<div class="col-md-3"/></div>
	</div>
</div>