<div class="container perso-container">

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $controller->alertVentes; ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		   	<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<h1 class="center">Journaux</h1><br>

	<div class=" col-md-12 fom-perso-2">
		<div class="col-md-12 perso-form-1">
			<p class="center orange-text bold">	
				A partir d'ici tu peux enregistrer les ventes de journaux et les objectifs de ventes.<br>
				<form method="post" action="index.php?p=ventes">
						<input class="btn btn-success bloc-center" type="submit" name="ventes" value="Ventes"/>
				</form>
			</p>
		</div>
	</div>
	<br>
	<div class=" col-md-12 fom-perso-3">
		<div class="col-md-12 perso-form-1">
			<p class="center orange-text bold">
				Consulter les bilans bimensuels ou annuels des ventes de journaux.<br>
				<form method="post" action="index.php?p=bilans">
						<input class="btn btn-warning bloc-center" type="submit" name="bilans" value="Bilans"/>
				</form>
			</p>
		</div>
	</div>
	<br>
	<div class=" col-md-12 fom-perso-4">
		<div class="col-md-12 perso-form-1">
			<p class="center orange-text bold">	
				Ou Voir les adhérants qui ne sont pas à jour dans leurs abonnements au journal.<br>
				<form method="post" action="index.php?p=abonnements">
						<input class="btn btn-danger bloc-center" type="submit" name="abonnements" value="Abonnements"/>
				</form>
			</p>
		</div>
	</div>		
</div>
