<!-- Modal -->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="confirm-delete">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <h4 class="modal-title" id="myModalLabel">Voulez-vous vraiement supprimer cet adhérant?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="btn-no">Non</button>
        <button type="button" class="btn btn-primary" id="btn-yes">Oui</button>
      </div>
    </div>
  </div>
</div>

<!-- Page -->

<div class="perso-container-2">

	<h1 class="center">Liste des Adhérants</h1><br>

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $alertModifSuppAdh; ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div class="row">
		<div class="col-md-12 perso-form-1">
			<div class="col-md-12">
				<a href="index.php?p=liste_adh"><input type="submit" class="btn btn-dark bloc-center" value="Retour" /></a><br>
			</div>
			<div class="input-group md-form form-sm form-1 pl-0 col-md-12 center">
			  	<div class="input-group-prepend">
			    	<span class="input-group-text search" id="basic-text1">
			    		<i class="fas fa-search text-white"
			        	aria-hidden="true">
			        	</i>
			        </span>
			  	</div>
			  	<input id="search" class="form-control my-0 py-1" type="text" placeholder="Search" aria-label="Search">
			</div>
			<br>
			<div class="table-responsive">
				<table id="adh_table" class="table center">
					<thead>
						<tr>
							<th scope="col">ID</th>
							<th scope="col">Nom</th>
							<th scope="col">Prénom</th>
							<th scope="col">Adresse</th>
							<th scope="col">Téléphone</th>
							<th scope="col">E-mail</th>
							<th scope="col">Abonnement Journal</th>
							<th scope="col">Date d'abonnement</th>
							<th scope="col">Date fin d'abonnement</th>
							<th scope="col">Cotisation par</th>
							<th scope="col"></th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody>
						<?php
						for ($i = 0; $i < count($data_adherants); $i++) {
						?>
						
							<tr>
								<th scope="row"><?=  $data_adherants[$i]['id'] ?></th>
								<td class="nom"><?=  $data_adherants[$i]['nom'] ?></td>
								<td class="prenom"><?=  $data_adherants[$i]['prenom'] ?></td>
								<td><?=  $data_adherants[$i]['adresse_postale'] ?></td>
								<td><?=  $data_adherants[$i]['telephone'] ?></td>
								<td><?=  $data_adherants[$i]['mail'] ?> </td>
								<td><?php  if($data_adherants[$i]['abonnement'] === '1'){ echo'oui';}else{echo "non";} ?></td>
								<td><?= date("d.m.Y", strtotime($data_adherants[$i]['date_abonnement'])) ?></td>
								<td><?=  date("d.m.Y", strtotime($data_adherants[$i]['date_fin'])) ?></td>
								<td><?php  if(empty($data_adherants[$i]['mode_paiement_coti'])){echo'non';}else{echo $data_adherants[$i]['mode_paiement_coti'];} ?></td>
								<td>
									<form method="post" action="#">
										<input type="hidden" name="id" value="<?php echo $data_adherants[$i]['id'] ; ?>">
										<input type="submit" class="btn btn-primary" name="modifier" value="Modifier">
									</form>
								</td>
								<td>
									<form method="post" action="#">
										<input type="hidden" name="id_delete" value="<?php echo $data_adherants[$i]['id'] ; ?>">
										<input type="hidden" name="btn_supp_adh" value="btn">
										<input type="submit" class="btn btn-danger" name="supprimmer_adh"value="Supprimmer">
									</form>
								</td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
