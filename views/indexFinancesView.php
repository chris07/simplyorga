<div class="container perso-container">
	<h1 class="center">Finances</h1><br>
	<div class=" col-md-12 fom-perso-2">
		<div class="col-md-12 perso-form-1">
			<p class="center orange-text bold">
				A partir d'ici tu enregistrer les cotisations d'un adhérent.<br>
				<form method="post" action="index.php?p=saisie_cotisations">
					<input class="btn btn-success bloc-center" type="submit" name="saisie_cotisations" value="Saisie Cotisations"/>
				</form>
			</p>
		</div>
	</div>
	<br>
	<div class=" col-md-12 fom-perso-3">
		<div class="col-md-12 perso-form-1">
			<p class="center orange-text bold">
				Ou voir le recap des cotisations de tes adhérents.<br>
				<form method="post" action="index.php?p=liste_coti">
					<input class="btn btn-primary bloc-center" type="submit" name="recap_cotisations" value="Recap Cotisations"/>
				</form>
			</p>
		</div>
	</div>
	<br>
	<div class=" col-md-12 fom-perso-4">
		<div class="col-md-12 perso-form-1">
			<p class="center orange-text bold">
				Ou gérer le fond de lutte de l'orga.<br>
				<form method="post" action="index.php?p=fond_lutte">
					<input class="btn btn-danger bloc-center" type="submit" name="fond" value="Fond de lutte"/>
				</form>
			</p>	
		</div>
	</div>
</div>