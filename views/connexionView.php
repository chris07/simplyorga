<div class="container perso-container">

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $alertConnexion; ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true">&times;</span>
	  	</button>
	</div>

	<div class="row">
		<div class="col-md-3"/></div>
		<div class="col-md-6 perso-form-1">
			<form method="post" action="#">
				<fieldset>
					<legend>Connexion</legend>
					<div class="form-group">
						<label for="login" class="form-label">login :</label>
						<input name="login" type="text" id="login" class="form-control" /><br />
					</div>
					<div class="form-group">
						<label for="password" class="form-label">Mot de Passe :</label>
						<input type="password" name="password" id="password" class="form-control" />
					</div>
				</fieldset>
				<div class="form-group">
					<input type="submit" name="send" value="Connexion" class="btn btn-primary" />
				</div>
			</form>
		</div>
		<div class="col-md-3"/></div>
	</div>
	
</div>