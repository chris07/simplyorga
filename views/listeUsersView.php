<!-- Modal -->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="confirm-delete">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <h4 class="modal-title" id="myModalLabel">Voulez-vous vraiement supprimer cet utilisateur</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="btn-no">Non</button>
        <button type="button" class="btn btn-primary" id="btn-yes">Oui</button>
      </div>
    </div>
  </div>
</div>

<!-- Page -->

<div class="container perso-container">

	<h1 class="center">Liste des Utilisateurs</h1><br>

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $controller->alertModifSuppUser; ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true">&times;</span>
	  	</button>
	</div>

	<div class="row">
		<div class="col-md-12 perso-form-1">
			<div class="flex">
				<div class="part1"></div>
				<div class="part2"><a href="index.php?p=liste_users"><input type="submit" class="btn btn-dark center" value="Retour" /></a><br></div>
				<div class="part3"></div>
			</div>
			<br>
			<div class="table-responsive">
				<table class="table center">
					<thead>
						<tr>
							<th scope="col">ID</th>
							<th scope="col">Nom</th>
							<th scope="col">Prénom</th>
							<th scope="col">Login</th>
							<th scope="col"></th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody>
						<?php
						for ($i = 0; $i < count($controller->data); $i++) {
						?>
						
							<tr>
								<th scope="row"> <?=  $controller->data[$i]['id'] ?> </th>
								<td> <?=  $controller->data[$i]['nom'] ?> </td>
								<td> <?=  $controller->data[$i]['prenom'] ?> </td>
								<td> <?=  $controller->data[$i]['login'] ?> </td>
								<td>
									<?php if(is_admin()){ ?>
									<form class="modif_form" method="post" action="#">
										<input type="hidden" name="id" value="<?php echo $controller->data[$i]['id'] ; ?>">
										<input type="submit" class="btn btn-primary" name="modifier" value="Modifier">
									</form>
									<?php } ?>
								</td>
								<td>
									<?php if(is_admin()){ ?>
									<form class="supp_form" method="post" action="#">
										<input type="hidden" name="id_delete" value="<?php echo $controller->data[$i]['id'] ; ?>">
										<input type="hidden" name="btn_supp_user" value="btn">
										<input type="submit" class="btn btn-danger" name="supprimmer" value="Supprimmer">
									</form>
									<?php } ?>
								</td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>