<?php

require_once  ROOT_PATH .'/app/Load_Script.php';

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= isset($title) ? $title : 'SimplyOrga'; ?></title>
    <link rel="icon" sizes="144x144" href="img/icon.png">
    <link rel="stylesheet" type="text/css" href="https://simplyorga.sky-zoo.fr/lib/bootstrap/bootstrap-4.2.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="sass/style.css">
    <script type="text/javascript" src="https://simplyorga.sky-zoo.fr/lib/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://simplyorga.sky-zoo.fr/lib/bootstrap/bootstrap-4.2.1-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://simplyorga.sky-zoo.fr/node_modules/chart.js/dist/Chart.bundle.js"></script>
</head>
<body>
    
    
    <header>
        <div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-fix">
                
                <?php require_once  ROOT_PATH .'/views/template_Parts/nav.php'; ?>
                
            </nav>
        </div>
    </header>
    
    <div id='content'>

        <?= $content; ?>

    </div>

    <footer>

        <?php require_once  ROOT_PATH .'/views/template_Parts/footer.php'; ?>

    </footer>
   

    <?= Load_Script::getInstance()->display_scripts(); ?>


</body>
</html>
