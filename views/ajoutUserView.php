
<!-- page -->

<div class="container perso-container">

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $controller->alertAjoutUser; ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true">&times;</span>
	  	</button>
	</div>

	<h1 class="center">Formulaire d'ajout d'un Utilisateur</h1><br>

	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8 perso-form-1">
			<h4 class="center bold">Nouvel Utilisateur</h4><br>
			<form method="post" action="#">
				<div class="form-group row center">
					<label class="col-sm-2 col-form-label" for="nom">Nom :</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="nom" id="nom" value="<?php if(isset($_POST['nom'])){echo htmlspecialchars($_POST['nom']);} ?>">
					</div>
				</div>
				<div class="form-group row center">
					<label class="col-sm-2 col-form-label" for="prenom">Prénom :</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="prenom" id="prenom" value="<?php if(isset($_POST['prenom'])){echo htmlspecialchars($_POST['prenom']);} ?>">
					</div>
				</div>
				<div class="form-group row center">
					<label class="col-sm-2 col-form-label" for="login">Login :</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="login" id="login" value="<?php if(isset($_POST['login'])){echo htmlspecialchars($_POST['login']);} ?>">
					</div>
				</div>
				<div class="form-group row center">
					<label class="col-sm-2 col-form-label" for="password">Mot de passe :</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="password" id="password">
					</div>
				</div>
				<br>
				<div class="form-group row center">
			    	<div class="col-sm-12">
						<input class="btn btn-primary" type="submit" name="new_user" value="Enregistrer">
					</div>
				</div>
			</form>
			<div class="col-md-12">
				<a href="index.php?p=liste_users"><input type="submit" class="btn btn-dark bloc-center" value="Retour" /></a><br>
			</div>
		</div>
		<div class="col-md-2"/></div>
	</div>
</div>




