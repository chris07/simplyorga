<div class="container perso-container">

	<h1 class="center">Liste des abonnements expirés</h1><br>
	
	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $controller->alertabonn ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div class="row">
		<div class="col-md-12 perso-form-1">
			<div class="flex">
				<div class="part1"></div>
				<div class="part2"><a href="index.php?p=journaux"><input type="submit" class="btn btn-dark center" value="Retour" /></a><br></div>
				<div class="part3"></div>
			</div>
			<br>
			<div class="table-responsive">
				<table class="table center">
					<tr>
						<td>Nom</td>
						<td>Prénom</td>
						<td>e-mail</td>
						<td>Fin d'abonnement</td>
						<td></td>
						<td></td>
						
					</tr>
					<?php
					foreach ($controller->data as $result) {
					?>
					
						<tr>
							<td> <?= $result['nom'] ?> </td>
							<td> <?= $result['prenom'] ?> </td>
							<td> <?= $result['e-mail'] ?> </td>
							<td> <?= date("d.m.Y", strtotime($result['date_fin'])) ?> </td>
							<td> <input class="btn btn-success renouv"  type="button" name="renouveler" value="Renouveler"> </td>
							<td class="dnone renouv-form">
								<form method="post" action="#">
									<div class="form-group row center">
										<input class="form-control col-md-6" type="date" name="date_new_abonn" >
								 		<input class="btn btn-warning col-md-6" type="submit" name="modif_date" value="Enregistrer">	
									</div>
								 	<input type="hidden" name="adh_id" value='<?php echo $result["id"] ;?>'>
								</form>
							</td>
						</tr>
					<?php
					}
					?>
				</table>
			</div>
		</div>
	</div>
</div>

