
<!-- Modal -->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="confirm-update">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <h4 class="modal-title" id="myModalLabel">Voulez-vous vraiement modifier cet utilisateur</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="btn-no">Non</button>
        <button type="button" class="btn btn-primary" id="btn-yes">Oui</button>
      </div>
    </div>
  </div>
</div>

<!-- Page -->

<div class="container perso-container">

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $controller->alertModifUser; ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true">&times;</span>
	  	</button>
	</div>

	<h1 class = "center">Formulaire de modification d'un Utilisateur</h1><br>

	<div class="row">
		<div class="col-md-3"/></div>
		<div class="col-md-6 perso-form-1">
			<h4 class="center bold">Utilisateur</h4><br>
			<div class="center">
				<h5 class="bold"><?= $controller->data[0]['login'] ?></h5><br>
			</div>
			<form method="post" action="#">
				<div class="form-group row center">
					<label for="nom" class="col-sm-3 col-form-label">Nom :</label>
					<div class="col-sm-6">
						<input type="text" name="nom" class="form-control form-control-sm" id="nom" value="<?php echo $controller->data[0]['nom'] ; ?>">
					</div>
				</div>	
				<div class="form-group row center">
					<label for="prenom" class="col-sm-3 col-form-label">Prénom :</label>
					<div class="col-sm-6">			
						<input type="text" name="prenom" class="form-control form-control-sm" id="prenom" value="<?php echo $controller->data[0]['prenom'] ; ?>">
					</div>
				</div>
				<div class="form-group row center">
					<label for="login" class="col-sm-3 col-form-label">Login :</label>
					<div class="col-sm-6">
						<input type="text" name="login" class="form-control form-control-sm" id="login" value="<?php echo $controller->data[0]['login'] ; ?>">
					</div>
				</div>
				<div class="form-group row center">	
					<input type="hidden" name="id" value="<?php echo $controller->data[0]['id'] ; ?>">
					<input type="hidden" name="bouton_modif_user" value="bouton_modifier">
					<div class="col-sm-12">	
						<input type="submit" class="btn btn-primary"name="update_user" value="Modifier">
					</div>
					<br>
				</div>
			</form>
			<div class="col-sm-12">
				<form method="post" action="#">
					<input type="submit" name="liste_users" class="btn btn-dark bloc-center" value="Retour" /><br>
				</form>
			</div>		
		</div>
		<div class="col-md-3"/></div>
</div>


