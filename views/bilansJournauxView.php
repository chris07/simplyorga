<div class="container perso-container">

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?=  $controller->alertbilan ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  	<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<h1 class="center">Bilans des ventes de journaux par rapport aux objectifs</h1><br>

	<div class="row">
		<div class="col-md-1"/></div>
		<div class="col-md-10 perso-form-1">
			<div class="col-md-12">
				<a href="index.php?p=Journaux"><input type="submit" class="btn btn-dark bloc-center" value="Retour" /></a><br>
			</div>
			<br>
			<div id="an">
				<form method="post" name="search" id="search" action="#">
					<div class="form-group row center">
						<label class="col-sm-12 col-form-label" for="search">Année</label>
						<div class="col-sm-12 bloc-center">
							<select class="custom-select bloc-center" id="year" name="year">
								<option></option>
								<option>2018</option>
								<option>2019</option>
								<option>2020</option>
								<option>2021</option>
								<option>2022</option>
								<option>2023</option>
								<option>2024</option>
								<option>2025</option>
								<option>2026</option>
								<option>2027</option>
								<option>2028</option>
								<option>2029</option>
								<option>2030</option>
								<option>2031</option>
							</select><br>
						</div>
					</div>				
					<button class="btn btn-primary bloc-center" type="submit" name="submit" id="submit">Afficher</button><br>
				</form>
			</div>
			<div id="container">
				<canvas id="canvas"></canvas>
				<script type="text/javascript"> <?=  $controller->graphs[0]  ?> </script>
			</div>
			<br>
			<br>
			<div class="row">
				<div id="container-pie" class="col-md-6">
					<canvas id="canvas-pie"></canvas>
					<script type="text/javascript"> <?=  $controller->graphs[1]  ?> </script>
				</div>
				<div id="container-pie2" class="col-md-6">
					<canvas id="canvas-pie2"></canvas>
					<script type="text/javascript"> <?=  $controller->graphs[2]  ?> </script>
				</div>
			</div>
			<br>
			<br>
			<div id="container-line1">
					<canvas id="canvas-line"></canvas>
					<script type="text/javascript"> <?=  $controller->graphs[3]  ?> </script>
				</div>
		</div>
		<div class="col-md-1"/></div>
	</div>
</div>


