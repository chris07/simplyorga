<div class="container perso-container">
	<h1 class="center">Fond de lutte</h1><br>

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $alertFond ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div class="row">
		<div class="col-md-12 perso-form-1">
			<form method="post" action="#">
				<input class="btn btn-success bloc-center" type="submit" name="new_transac" value="Ajouter une transaction"><br>
				<input type="hidden" name="solde" value="<?php echo $solde; ?>">
			</form>
			<div class="solde">
				<p class="center bold">
					Solde :  <span class="montant-solde"><?= $solde ?> €</span>
				</p>	
			</div>
			<h4 class="center bold">Liste des Transactions</h4><br>
			<?php  
			foreach ($data as $transac) {
			?>
				<div class="transac center">
					<div class="detail-transac"><?= date("d.m.Y", strtotime($transac['date'])) ?></div>
					<div class="detail-transac"><?= $e->get_cat_name(intval($transac['id_sous_cat'])); ?></div>
					<div class="detail-transac"><?php if($transac['type'] ==='recette'){  echo '<span class="recette"> + ' ;}else{ echo '<span class="depence"> - ';} ?>
					<?= $transac['montant'] ?></span></div>
					<div class="detail-transac"><?= $transac['commentaire'] ?></div>
				</div>
			<?php
			}
			?>
		</div>
	</div>
</div>