<div class="container perso-container">

	<h1 class="center">Cotisations</h1><br>

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?=  $alertcoti ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8 perso-form-1">
			<h3 class="center">Cotisation</h3>
			<div>
				<form method="post" action="#">
					<div class="form-group row center">
						<label class="col-sm-12 col-form-label" for="adh">Choix de l'hadérent:</label>
						<div class="col-sm-12 bloc-center">
							<select class="custom-select bloc-center" id="adh" name="adh">
								<option></option>
								<?php  
								foreach($users as $user){
								?>
								<option class="form-check-input" value="<?php echo $user['id'] ?>"><?= $user['nom'].' '.$user['prenom'] ?></option>
								<?php
								}
								?>
							</select><br>
						</div>
					</div>
					<div class="form-group row center">
						<label class="col-sm-12 col-form-label" for="year">Choix de l'année:</label>
						<div class="col-sm-12 bloc-center">
							<select class="custom-select bloc-center" id="year" name="year">
								<option ></option>
								<option >2018</option>
								<option >2019</option>
								<option >2020</option>
								<option >2021</option>
								<option >2022</option>
								<option >2023</option>
								<option >2024</option>
								<option >2025</option>
								<option >2026</option>
								<option >2027</option>
								<option >2028</option>
								<option >2029</option>
								<option >2030</option>
								<option >2031</option>
							</select><br>
						</div>
					</div>
					<button class="btn btn-success bloc-center" type="submit" name="submit_coti" id="submit">Afficher</button><br>
					<div class="col-md-12">
						<a href="index.php?p=Journaux"><input type="submit" class="btn btn-dark bloc-center" value="Retour" /></a><br>
					</div>
				</form>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
</div>
			





