
<!-- Modal -->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="confirm-update">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <h4 class="modal-title" id="myModalLabel">Voulez-vous vraiement modifier cet adhérant?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="btn-no">Non</button>
        <button type="button" class="btn btn-primary" id="btn-yes">Oui</button>
      </div>
    </div>
  </div>
</div>

<!-- Page -->

<div class="container perso-container">

	<h1 class="center">Formulaire de modif adhérant</h1><br>

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $alertModifAdh; ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		   	<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div class="row">
		<div class="col-md-2"/></div>
		<div class="col-md-8 perso-form-1">
			<h4 class="center bold"><?= $data_adh[0]['nom']. " " .$data_adh[0]['prenom'] ?></h4><br>
			<form method="post" action="#">
				<fieldset>
					<legend class="center bold">Coordonnées</legend>
					<div class="form-group row center">
						<label class="col-sm-3 col-form-label" for="nom">Nom :</label>
						<div class="col-sm-7">
							<input class="form-control" type="text" name="nom" id="nom" value="<?php echo $data_adh[0]['nom'] ; ?>">
						</div>
					</div>
					<div class="form-group row center">
						<label class="col-sm-3 col-form-label" for="prenom">Prénom :</label>
						<div class="col-sm-7">
							<input class="form-control" type="text" name="prenom" id="prenom" value="<?php echo $data_adh[0]['prenom'] ; ?>">
						</div>
					</div>
					<div class="form-group row center">
						<label class="col-sm-3 col-form-label" for="adresse">Adresse :</label>
						<div class="col-sm-7">	
							<input class="form-control" type="text" name="adresse" id="adresse" value="<?php echo $data_adh[0]['adresse_postale'] ; ?>">
						</div>
					</div>
					<div class="form-group row center">
						<label class="col-sm-3 col-form-label" for="tel">Téléphone :</label>
						<div class="col-sm-7">	
							<input class="form-control" type="tel" name="tel" id="tel" value="<?php echo $data_adh[0]['telephone'] ; ?>">
						</div>
					</div>
					<div class="form-group row center">
						<label class="col-sm-3 col-form-label" for="mail">e-mail :</label>
						<div class="col-sm-7">	
							<input class="form-control" type="email" name="mail" id="mail" value="<?php echo $data_adh[0]['mail'] ; ?>">
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend class="center bold">Abonnement au journal</legend>
					<p class="center">
						Cet adhérant est-il abonné au journal?<br/>
					</p>
					<div class="bloc-center">
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="abonnement" value="oui" id="abonne" <?php if($data_adh[0]['abonnement'] === '1'){echo 'checked' ;} ?> />
							<label class="form-check-label" for="abonne">Oui</label><br />
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="abonnement" value="non" id="non_abonne" <?php if($data_adh[0]['abonnement'] === '0'){echo 'checked' ;} ?> />
							<label class="form-check-label" for="non_abonne">Non</label><br />
						</div>
						<div class="form-group row bloc-center modif_date">
							<label class="col-sm-4 col-form-label" for="date">Date d'abonnement :</label><br>
							<div class="col-sm-4 bloc-center">
								<input class="form-control" type="date" name="date" id="date" value="<?php echo $data_adh[0]['date_abonnement'] ; ?>"><br>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend class="center bold">Cotisation</legend>
						<p class="center">
							Cet adhérant cotise t'il?<br/>
						</p>
						<div class="bloc-center">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="cotisation" value="1" id="cotise" <?php if($data_adh[0]['cotisation'] === '1'){echo 'checked' ;} ?> />
								<label class="form-check-label" for="cotise">Oui</label><br />
								<input class="form-check-input" type="radio" name="cotisation" value="0" id="non_cotise" <?php if($data_adh[0]['cotisation'] === '0'){echo 'checked' ;} ?> />
								<label class="form-check-label" for="non_cotise">Non</label><br />
							</div>
						</div>
						<div class="form-group row bloc-center modif_coti">
							<label class="col-sm-4 col-form-label center" for="coti">Mode de paiement de la cotisation ?</label><br/>
							<div class="col-sm-12 bloc-center">
								<select class="custom-select col-sm-4 bloc-center" name="coti" id="coti">
									<option value=""></option>
									<option value="espece" <?php if($data_adh[0]['mode_paiement_coti'] === 'espece'){echo 'selected' ;} ?> >Espèce</option>
									<option value="cheque" <?php if($data_adh[0]['mode_paiement_coti'] === 'cheque'){echo 'selected' ;} ?> >Chèque</option>
									<option value="virement" <?php if($data_adh[0]['mode_paiement_coti'] === 'virement'){echo 'selected' ;} ?> >Virement Bancaire</option>
								</select>
							</div><br>	
						</div>
				</fieldset>
				<input type="hidden" name="id" value="<?php echo $data_adh[0]['id'] ; ?>">
				<input type="hidden" name="bouton_modif_adh" value="bouton_modifier">
				<input  class="btn btn-primary bloc-center" type="submit" name="update_adh"value="Modifier">
			</form><br>
			<div class="col-sm-12">
				<form method="post" action="#">
					<input type="submit" name="liste_adh" class="btn btn-dark bloc-center" value="Retour" /><br>
				</form>
			</div>	
		</div>
		<div class="col-md-2"/></div>
	</div>	
</div>

