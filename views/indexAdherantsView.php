<div class="container perso-container">
	<h1 class="center">Adhérants</h1><br>
	<div class=" col-md-12 fom-perso-2">
		<div class="col-md-12 perso-form-1">
			<p class="center orange-text bold">
				A partir d'ici tu peux ajouter des adhérantspour l'orga.<br>
				<form method="post" action="index.php?p=ajout_adh">
					<input type="submit" class="btn btn-success bloc-center" name="ajout_adh" value="Ajouter un Adhérant"/>
				</form>
			</p>
		</div>
	</div>
	<br>
	<div class="col-md-12 fom-perso-3">
		<div class="col-md-12 perso-form-1">
			<p class="center orange-text bold">
				Ou voir la liste des adhérants pour les modifier ou les supprimmer.<br>
				<form method="post" action="index.php?p=liste_adh">
					<input type="submit" class="btn btn-primary bloc-center" name="liste_adh" value="Liste des Adhérants"/>
				</form>
			</p>	
		</div>
	</div>
</div>
