<div class="container perso-container">
	<h1 class="center">Paramètres</h1><br>
	<div class=" col-md-12 fom-perso-2">
		<div class="col-md-12 perso-form-1">
			<p class="center orange-text bold">
				A partir d'ici tu peux ajouter des utilisateurs pour le logiciel.<br>
				<form method="post" action="index.php?p=ajout_user">
					<input type="submit" class="btn btn-success bloc-center" name="ajout_user" value="Ajouter un Utilisateur"/>
				</form>
			</p>
		</div>
	</div>
	<br>
	<div class=" col-md-12 fom-perso-3">
		<div class="col-md-12 perso-form-1">
			<p class="center orange-text bold">	
				Ou voir la liste des utilisateurs pour les modifier ou les supprimmer.<br>
				<form method="post" action="index.php?p=liste_users">
					<input type="submit" class="btn btn-primary bloc-center" name="liste_users" value="Liste des Utilisateurs"/>
				</form>
			</p>
		</div>
	</div>
	<br>
	<div class=" col-md-12 fom-perso-4">
		<div class="col-md-12 perso-form-1">
			<p class="center orange-text bold">	
				Ou encore voir la liste des catégories ainsi que les modifier ici.<br>
				<form method="post" action="index.php?p=liste_categories">
					<input type="submit" class="btn btn-primary bloc-center" name="liste_categories" value="Liste des Catégories"/>
				</form>
			</p>
		</div>
	</div>		
</div>
