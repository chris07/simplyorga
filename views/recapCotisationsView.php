<div class="perso-container-2">

	<h1 class="center">Cotisations de l'année en cours</h1><br>

	<div class="row">
		<div class="col-md-12 perso-form-1">
			<div class="col-md-12">
				<a href="index.php?p=finances"><input type="submit" class="btn btn-dark bloc-center" value="Retour" /></a><br>
			</div>
			<div class="table-responsive">
				<table class="table center">
					<tr>
						<td class="bold">Nom</td>
						<td class="bold">Année</td>
						<td class="bold">Janvier</td>
						<td class="bold">Février</td>
						<td class="bold">Mars</td>
						<td class="bold">Avril</td>
						<td class="bold">Mai</td>
						<td class="bold">Juin</td>
						<td class="bold">Juillet</td>
						<td class="bold">Août</td>
						<td class="bold">Septembre</td>
						<td class="bold">Octobre</td>
						<td class="bold">Novembre</td>
						<td class="bold">Décembre</td>
					</tr>
					<?php for($i = 0; $i <= count($users)-1; $i++){
					?>
					<tr>
						<td><?= $users[$i]['nom']. " " . $users[$i]['prenom'] ?></td>
						<td><?= $year ?></td>
						<td class="nb"><?= $data[$i]['janvier'] ?></td>
						<td class="nb"><?= $data[$i]['fevrier'] ?></td>
						<td class="nb"><?= $data[$i]['mars'] ?></td>
						<td class="nb"><?= $data[$i]['avril'] ?></td>
						<td class="nb"><?= $data[$i]['mai'] ?></td>
						<td class="nb"><?= $data[$i]['juin'] ?></td>
						<td class="nb"><?= $data[$i]['juillet'] ?></td>
						<td class="nb"><?= $data[$i]['aout'] ?></td>
						<td class="nb"><?= $data[$i]['septembre'] ?></td>
						<td class="nb"><?= $data[$i]['octobre'] ?></td>
						<td class="nb"><?= $data[$i]['novembre'] ?></td>
						<td class="nb"><?= $data[$i]['decembre'] ?></td>
					</tr>
					<?php
					}
					?>
				</table>
			</div>
		</div>
	</div>
</div>


