
<!-- Modal -->

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="confirm-add">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <h4 class="modal-title" id="myModalLabel">Voulez-vous vraiement Enregistrer cette transaction ? <span style="color:red;">Cette action est définitive!</span></h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="btn-no">Non</button>
        <button type="button" class="btn btn-primary" id="btn-yes">Oui</button>
      </div>
    </div>
  </div>
</div>

<!-- Page -->

<div class="container perso-container">
	<h1 class="center">Ajout d'une transaction</h1><br>

	<div class="alert alert-dark alert-dismissible fade show dnone" role="alert">
		<p class="center"><?= $alertAddTransac ?></p>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div class="row">
		<div class="col-md-2"/></div>
		<div class="col-md-8 perso-form-1">
			<h3 class="center bold">Nouvelle Transaction</h3>
			<form method="post" action="#">
				<div class="form-group row center">
					<label class="col-sm-12 col-form-label bold" for="date_transac">Date</label>
					<div class="col-sm-6 bloc-center">
						<input class="form-control" type="date" name="date_transac" id="date_transac">		
					</div>		
				</div>
				<br>
				<h4 class="center bold">Catégorie</h4>
				<div class="form-group row center">
					<label class="col-sm-12 col-form-label center" for="categorie">Catégorie de la transaction</label><br/>
					<div class="col-sm-12 bloc-center">
						<select class="custom-select col-sm-4 bloc-center" name="categorie" id="categorie">
							<option value=""></option>
							<!--pour chaque cat affiche l'ooption -->
							<?php  
							foreach ($cats as $cat ) {
								
							?>
							<option value="<?php echo $cat['nom']; ?>"><?= $cat['nom'] ?></option>
							<?php
							}  
							?>
						</select><br>
					</div>
				</div>
				<br>
				<div class="bloc-center">
					<h4 class="center bold">Type</h4>
					<p class="center">S'agit'il d'une dépence ou d'une recette?</p><br/>
					<div class="form-check center">
						<input class="form-check-input" type="radio" name="type" value="recette" id="recette" />
						<label class="form-check-label" for="recette">Recette</label>
					</div>
					<div class="form-check center">
						<input class="form-check-input" type="radio" name="type" value="depence" id="depence" />
						<label class="form-check-label" for="depence">Dépence</label>
					</div>
				</div>
				<br>
				<h4 class="center bold">Montant</h4>
				<div class="form-group row center">
					<label class="col-sm-12 col-form-label" for="montant">Montant de la transaction</label>
					<div class="col-sm-6 bloc-center">
						<input class="form-control" type="number" name="montant" id="montant" step="0.01"><br />
					</div>
				</div>
				<h4 class="center bold">Commentaire</h4>
				<div class="form-group row center">
					<label class="col-sm-12 col-form-label" for="commentaire">Exemples ( Lieu, Remonté trésorerie ... )</label>
					<div class="col-sm-8 bloc-center">
						<textarea class="form-control" name="commentaire" id="commentaire"></textarea><br />
					</div>
				</div>
				<div class="form-group row center">
					<input type="hidden" name="solde" value="<?php echo $solde;  ?>">
					<input type="hidden" name="bouton_add_transac" value="bouton_ajouter">
					<input class="btn btn-danger bloc-center" type="submit" name="add_transac" value="Enregistrer">
				</div>
				<br>
				<div class="form-group row center">
					<a class="bloc" href="index.php?p=fond_lutte">
						<input type="submit" class="btn btn-dark bloc-center" value="Retour" />
					</a>
				</div>
			</form>
		</div>
		<div class="col-md-2"/></div>
	</div>
</div>
