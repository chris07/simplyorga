-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  db5000084878.hosting-data.io
-- Généré le :  Ven 24 Mai 2019 à 13:06
-- Version du serveur :  5.7.26-log
-- Version de PHP :  7.0.33-0+deb9u3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dbs79659`
--

-- --------------------------------------------------------

--
-- Structure de la table `adherants`
--

CREATE TABLE `adherants` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse_postale` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abonnement` tinyint(1) NOT NULL,
  `date_abonnement` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `cotisation` tinyint(1) NOT NULL,
  `mode_paiement_coti` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`) VALUES
(1, 'matériel'),
(2, 'évènementiel');

-- --------------------------------------------------------

--
-- Structure de la table `cotisations`
--

CREATE TABLE `cotisations` (
  `id_adh` int(11) NOT NULL,
  `an` year(4) NOT NULL,
  `mois` int(2) NOT NULL,
  `montant` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fond_de_lutte`
--

CREATE TABLE `fond_de_lutte` (
  `id_transac` int(11) NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_sous_cat` int(11) NOT NULL,
  `montant` decimal(10,2) NOT NULL,
  `date` date NOT NULL,
  `commentaire` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `solde` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `fond_de_lutte`
--

INSERT INTO `fond_de_lutte` (`id_transac`, `type`, `id_sous_cat`, `montant`, `date`, `commentaire`, `solde`) VALUES
(3, 'recette', 15, '2000.35', '2019-05-19', 'remise', '2000.35'),
(4, 'recette', 8, '0.21', '2019-05-19', 'test', '2000.56'),
(5, 'recette', 8, '0.21', '2019-05-19', 'test', '2000.56'),
(6, 'recette', 8, '0.21', '2019-05-19', 'test', '2000.56'),
(7, 'recette', 10, '1.00', '2019-05-20', 'fff', '2001.56'),
(8, 'recette', 7, '20.40', '2019-05-21', 'vente journx montbouchau', '2021.96'),
(9, 'depence', 9, '50.00', '2019-05-21', 'c cher carouf', '1971.96'),
(10, 'recette', 15, '500.00', '2019-05-23', 'commerce illégal', '2471.96');

-- --------------------------------------------------------

--
-- Structure de la table `sous_categories`
--

CREATE TABLE `sous_categories` (
  `id` int(11) NOT NULL,
  `id_cat` int(11) NOT NULL,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `sous_categories`
--

INSERT INTO `sous_categories` (`id`, `id_cat`, `nom`) VALUES
(1, 1, 'badges'),
(2, 1, 'affiches'),
(3, 1, '4 pages'),
(4, 1, 'flyers'),
(5, 1, 'suppléments'),
(6, 1, 'stickers'),
(7, 1, 'brochures'),
(8, 1, 'autres matériel'),
(9, 2, 'nourriture/boisson'),
(10, 2, 'dons'),
(11, 2, 'appels financiers'),
(12, 2, 'local'),
(13, 2, 'locations'),
(15, 2, 'autres évènementiel');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(20) NOT NULL,
  `nom` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `login`, `password`, `admin`) VALUES
(1, 'Bonhomme', 'Christopher', 'chris', '$2y$10$G4Qft0fucICtPl3X0Od7wu9NTd9HU1QO4flST1nZbvFPRHlBZ3Fv.', 1),
(2, 'admin', 'admin', 'admin', '$2y$10$G4Qft0fucICtPl3X0Od7wu9NTd9HU1QO4flST1nZbvFPRHlBZ3Fv.', 1);

-- --------------------------------------------------------

--
-- Structure de la table `vente_journaux`
--

CREATE TABLE `vente_journaux` (
  `an` year(4) NOT NULL,
  `semestre` int(1) NOT NULL,
  `montant` decimal(10,2) DEFAULT NULL,
  `objectif` int(10) DEFAULT NULL,
  `objectif_montant` decimal(10,2) DEFAULT NULL,
  `vendu` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `adherants`
--
ALTER TABLE `adherants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nom` (`nom`,`prenom`,`adresse_postale`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cotisations`
--
ALTER TABLE `cotisations`
  ADD UNIQUE KEY `id_adh_2` (`id_adh`,`an`,`mois`),
  ADD KEY `id_adh` (`id_adh`);

--
-- Index pour la table `fond_de_lutte`
--
ALTER TABLE `fond_de_lutte`
  ADD PRIMARY KEY (`id_transac`),
  ADD KEY `id_sous_cat` (`id_sous_cat`);

--
-- Index pour la table `sous_categories`
--
ALTER TABLE `sous_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cat` (`id_cat`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `adherants`
--
ALTER TABLE `adherants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `fond_de_lutte`
--
ALTER TABLE `fond_de_lutte`
  MODIFY `id_transac` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `sous_categories`
--
ALTER TABLE `sous_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `cotisations`
--
ALTER TABLE `cotisations`
  ADD CONSTRAINT `cotisations_ibfk_1` FOREIGN KEY (`id_adh`) REFERENCES `adherants` (`id`);

--
-- Contraintes pour la table `fond_de_lutte`
--
ALTER TABLE `fond_de_lutte`
  ADD CONSTRAINT `fond_de_lutte_ibfk_1` FOREIGN KEY (`id_sous_cat`) REFERENCES `sous_categories` (`id`);

--
-- Contraintes pour la table `sous_categories`
--
ALTER TABLE `sous_categories`
  ADD CONSTRAINT `sous_categories_ibfk_1` FOREIGN KEY (`id_cat`) REFERENCES `categories` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
