<?php

class Chart_Manager {
 

    
    
   

    public function barChart($nom, $xLabel, $dataset, $data){
       
            $xLabel = json_encode($xLabel);

            $jsChart = "
               
            var barChartData = {
            labels: ". $xLabel .",
            datasets: [{
                label: '". $dataset[0] ."',
                backgroundColor: 'rgba(25,255,64,0.7)',
                borderColor:  'rgba(0,102,17,0.7)', 
                borderWidth: 1,
                data: [
                    '". $data[0]['objectif'] ."',
                    '". $data[1]['objectif'] ."',
                    '". $data[2]['objectif'] ."',
                    '". $data[3]['objectif'] ."',
                    '". $data[4]['objectif'] ."',
                    '". $data[5]['objectif'] ."',
                    
                ]
            }, 
            {
                label: '". $dataset[1] ."',
                backgroundColor: 'rgba(0,128,255,0.7)',
                borderColor: 'rgba(0,60,179,0.7)',
                borderWidth: 1,
                data: [
                    '". $data[0]['vendu'] ."',
                    '". $data[1]['vendu'] ."',
                    '". $data[2]['vendu'] ."',
                    '". $data[3]['vendu'] ."',
                    '". $data[4]['vendu'] ."',
                    '". $data[5]['vendu'] ."',
                    
                ]
            },
            {
                label: '". $dataset[2] ."',
                backgroundColor: 'rgba(230,38,0,0.7)',
                borderColor: 'rgba(153,0,0,0.7)',
                borderWidth: 1,
                data: [
                    '". $data[0]['objectif_montant'] ."',
                    '". $data[1]['objectif_montant'] ."',
                    '". $data[2]['objectif_montant'] ."',
                    '". $data[3]['objectif_montant'] ."',
                    '". $data[4]['objectif_montant'] ."',
                    '". $data[5]['objectif_montant'] ."',
                ]
            },
            {
                label: '". $dataset[3] ."',
                backgroundColor: 'rgba(255,213,0,0.7)',
                borderColor: 'rgba(230,230,0,0.7)',
                borderWidth: 1,
                data: [
                    '". $data[0]['montant'] ."',
                    '". $data[1]['montant'] ."',
                    '". $data[2]['montant'] ."',
                    '". $data[3]['montant'] ."',
                    '". $data[4]['montant'] ."',
                    '". $data[5]['montant'] ."',
                ]
            }
            ]

        };

       
            var ctx = document.getElementById('canvas').getContext('2d');
            var myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        scales:{'yAxes':[{'ticks':{'beginAtZero':true}}]}
                    },
                    title: {
                        display: true,
                        text: '". $nom ."'
                    }
                }
            });

        ";

        return $jsChart;
    }


    public function lineChart( $xLabel, $dataset, $data){
       
            $xLabel = json_encode($xLabel);

            $jsChart = "
               
            var barChartData = {
            labels: ". $xLabel .",
            datasets: [{
                label: '". $dataset[0] ."',
                borderColor:  'rgba(25,255,64,0.7)', 
                backgroundColor: 'rgb(230,255,251,0)',
                borderWidth: 3,
                data: [
                    '". $data[0]['objectif'] ."',
                    '". $data[1]['objectif'] ."',
                    '". $data[2]['objectif'] ."',
                    '". $data[3]['objectif'] ."',
                    '". $data[4]['objectif'] ."',
                    '". $data[5]['objectif'] ."',
                    
                ]
            }, 
            {
                label: '". $dataset[1] ."',
                borderColor: 'rgba(0,128,255,0.7)',
                backgroundColor: 'rgb(230,255,251,0)',
                borderWidth: 3,
                data: [
                    '". $data[0]['vendu'] ."',
                    '". $data[1]['vendu'] ."',
                    '". $data[2]['vendu'] ."',
                    '". $data[3]['vendu'] ."',
                    '". $data[4]['vendu'] ."',
                    '". $data[5]['vendu'] ."',
                    
                ]
            },
            {
                label: '". $dataset[2] ."',
                borderColor: 'rgba(230,38,0,0.7)',
                backgroundColor: 'rgb(230,255,251,0)',
                borderWidth: 3,
                data: [
                    '". $data[0]['objectif_montant'] ."',
                    '". $data[1]['objectif_montant'] ."',
                    '". $data[2]['objectif_montant'] ."',
                    '". $data[3]['objectif_montant'] ."',
                    '". $data[4]['objectif_montant'] ."',
                    '". $data[5]['objectif_montant'] ."',
                ]
            },
            {
                label: '". $dataset[3] ."',
                borderColor: 'rgba(255,213,0,0.7)',
                backgroundColor: 'rgb(230,255,251,0)',
                borderWidth: 3,
                data: [
                    '". $data[0]['montant'] ."',
                    '". $data[1]['montant'] ."',
                    '". $data[2]['montant'] ."',
                    '". $data[3]['montant'] ."',
                    '". $data[4]['montant'] ."',
                    '". $data[5]['montant'] ."',
                ]
            }
            ]

        };

       
            var ctx2 = document.getElementById('canvas-line').getContext('2d');
            var myBar2 = new Chart(ctx2, {
                type: 'line',
                data: barChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        scales:{'yAxes':[{'ticks':{'beginAtZero':true}}]}
                    },
                }
            });

        ";

        return $jsChart;
    }


    public function pieChart( $id, $dataset, $data, $colors){
    

            $jsChart = "


                data = {
                        datasets: [{
                            data: [
                                '".$data[0]."', 
                                '".$data[1]."'
                            ],
                            backgroundColor: ['".$colors[0]."','".$colors[1]."']
                        }],

                        labels: [
                                '".$dataset[0]."',
                                '".$dataset[1]."'
                            ]
                    
                };
               
                var ctx3 = document.getElementById('".$id."').getContext('2d');
                var myPieChart = new Chart(ctx3, {
                    type: 'pie',
                    data: data,
                });
            ";

        return $jsChart;
    }

}
   