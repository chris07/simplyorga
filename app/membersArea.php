<?php

require_once ROOT_PATH .'models/usersModel.php';


/* 
 * fonction retournant un booléen si l'utilisateur est connecté
 * 
 * @return  bool                vrai ou faux
 */
function is_connected(){
    $value_return = false;
    if (isset($_SESSION['id_user'])){
        $value_return = true;
    }
    return $value_return;
}


function is_admin(){
    $value_return = false;
    if (isset($_SESSION['admin']) && $_SESSION['admin'] === 1){
        $value_return = true;
    }
    return $value_return;
}

/**
 * 
 * fonction de deconnexion de l'utilisateur
 * 
 */
function deconnection(){
    // suppression des variables de session
    session_unset ();
    //destruction de la session
    session_destroy ();
    //supression des variables de cookie
    //unset($_COOKIE['mot de passe haché']);
}

/**
 * 
 * fonction de connexion de l'utilisateur
 * 
 * @param $id               id de l'utilisateur
 * @param $password         mot de passe haché de l'utilisateur 
 */
function connexion( $id ){
    //demarage de la session => DÉJA FAIT DANS L'INDEX
    //session_start(); 
    //création des variables de session
    $_SESSION['id_user'] = $id; 

    $d = new usersModel();
    $admin = $d->admin($id);
 
    $_SESSION['admin'] = $admin; 
    //envoi des cookie
    //if (!isset($_COOKIE)){
    //setcookie("mot_de_passe_hache",$password,time()+3600*24*31);
    //setcookie("id_user",$id,time()+3600*24*31);
    //}
}

function get_ID_user() {
    $val_return = false;
    if( is_connected() ) $val_return = $_SESSION['id_user'];
    
    return $val_return;
}

function get_cats($data_categories){
    $cats = array();

        foreach ($data_categories as $souscat ) {
                
            if($souscat['id_cat'] === '1'){

                array_push($cats, 'matériel');
            }
            else{

                array_push($cats, 'évènementiel');
            }
        }

    return $cats;
}

