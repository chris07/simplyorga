	
<?php
	if (empty( $_POST['nom'] ) || empty( $_POST['prenom'] ) || empty( $_POST['login'] ) || empty( $_POST['password'] )) {
		
		$this->alertAjoutUser ='Tous les champs doivent être renseignés.';

		$this->vue = ROOT_PATH .'views/ajoutUserView.php';
	}
	else{

		$validation = new Data_Validation();
    	$password = $_POST['password'];
    	$login = $_POST['login'];
    	$nom = $_POST['nom'];
    	$prenom = $_POST['prenom'];

		if( ! $validation->verifTextNumber( $login ) ){

    	    $this->alertAjoutUser ='Le login ne peut contenir que des chiffres et des lettres';

        	$this->vue = ROOT_PATH .'views/ajoutUserView.php';
    	} 
    	elseif( ! $validation->verifTextNumber( $password ) ){

        	$this->alertAjoutUser ='Le mot de passe ne peut contenir que des chiffres et des lettres';

        	$this->vue = ROOT_PATH .'views/ajoutUserView.php';
    	}
    	elseif( ! $validation->verifText( $nom ) ){
    		
    	    $this->alertAjoutUser ='Le nom ne peut contenir que des lettres';

    	    $this->vue = ROOT_PATH .'views/ajoutUserView.php';
    	}
    	elseif( ! $validation->verifText( $prenom ) ){
    	
        	$this->alertAjoutUser ='Le prénom ne peut contenir que des lettres';

        	$this->vue = ROOT_PATH .'views/ajoutUserView.php';
    	}
    	else{

    		$d = new usersModel();
    		$validation->cancelSpecialChara( $prenom );
	    	$validation->cancelSpecialChara( $nom );
	    	$validation->cancelSpecialChara( $password );
	    	$validation->cancelSpecialChara( $login );
	    	$cryptPass = password_hash($password, PASSWORD_DEFAULT);
	    	if(! $d->add_user( $nom, $prenom, $login, $cryptPass ) ){

	    		$this->alertAjoutUser ="Le login est déjà utilisé";
	    		$this->vue = ROOT_PATH .'views/ajoutUserView.php';
	    	}
	    	else{

	    		$this->alertModifSuppUser ="L'utilisateur a bien été enregistré";
	    		$d = new usersModel();
	    		$this->data = $d->get_users();

	    		Load_Script::getInstance()->enqueue_script("js/confirm_supp_user.js");

				$this->vue = ROOT_PATH .'views/listeUsersView.php';
	    	}
		}
	}