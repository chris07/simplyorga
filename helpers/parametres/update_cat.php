<?php
$validation = new Data_Validation();
$id_update = $_POST['id'];

if( ! $validation->verifNumber( $id_update ) ){

	$this->alertModifSuppCategorie = 'Non mais tu t\'es cru où là?' ;

	Load_Script::getInstance()->enqueue_script("js/confirm_supp_cat.js");

	$this->vue = ROOT_PATH .'views/listeCategoriesView.php';
}
else{

	$d = new categoriesModel();
	$this->data = $d->get_cat($id_update);

	$this->alertModifSuppCategorie = '' ;

	Load_Script::getInstance()->enqueue_script("js/confirm_modif_cat.js");

	$this->vue = ROOT_PATH .'views/updateCatView.php';	
}