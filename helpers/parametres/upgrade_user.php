<?php   
$validation = new Data_Validation();
	$id_update = $_POST['id'];
    $login = $_POST['login'];
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];

	if( ! $validation->verifNumber( $id_update ) ){

		$d = new usersModel();
		$this->data = $d->get_users();
		$this->alertModifSuppUser = 'Non mais tu t\'es cru où là?' ;

		Load_Script::getInstance()->enqueue_script("js/confirm_supp_user.js");

		$this->vue = ROOT_PATH .'views/listeUsersView.php';
	}
	elseif(! $validation->verifText( $nom )){

		$d = new usersModel();
		$this->data = $d->get_user($id_update);
        $this->alertModifUser ='Le nom ne peut contenir que des lettres';

        Load_Script::getInstance()->enqueue_script("js/confirm_modif_user.js");

        $this->vue = ROOT_PATH .'views/updateUserView.php';
	}
	elseif(! $validation->verifText( $prenom )){

		$d = new usersModel();
		$this->data = $d->get_user($id_update);
        $this->alertModifUser ='Le prénom ne peut contenir que des lettres';

        Load_Script::getInstance()->enqueue_script("js/confirm_modif_user.js");

        $this->vue = ROOT_PATH .'views/updateUserView.php';
	}
    elseif(empty($nom) || empty($prenom) || empty($login)){

    	$d = new usersModel();
		$this->data = $d->get_user($id_update);
        $this->alertModifUser ="l'un des champs n'est pas renseigné";

        Load_Script::getInstance()->enqueue_script("js/confirm_modif_user.js");

        $this->vue = ROOT_PATH .'views/updateUserView.php';
    }
    elseif( ! $validation->verifTextNumber( $login ) ){

		$d = new usersModel();
		$this->data = $d->get_user($id_update);
        $this->alertModifUser ='Le login ne peut contenir que des chiffres et des lettres';

        Load_Script::getInstance()->enqueue_script("js/confirm_modif_user.js");

        $this->vue = ROOT_PATH .'views/updateUserView.php';
    }
	else{

		$d = new usersModel();
    	$validation->cancelSpecialChara( $prenom );
	    $validation->cancelSpecialChara( $nom );
	    $validation->cancelSpecialChara( $login );
	    $d->update_user( $id_update, $nom, $prenom, $login );
	    $this->alertModifSuppUser ="L'utilisateur a bien été Modifié";
	    $this->data = $d->get_users();

	    Load_Script::getInstance()->enqueue_script("js/confirm_supp_user.js");

	    $this->vue = ROOT_PATH .'views/listeUsersView.php';
	}