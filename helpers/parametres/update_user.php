<?php
$validation = new Data_Validation();
$id_update = $_POST['id'];

if( ! $validation->verifNumber( $id_update ) ){

	$this->alertModifSuppUser = 'Non mais tu t\'es cru où là?' ;

	Load_Script::getInstance()->enqueue_script("js/confirm_supp_user.js");

	$this->vue = ROOT_PATH .'views/listeUsersView.php';
}
else{

	$d = new usersModel();
	$this->data = $d->get_user($id_update);
	$this->alertModifUser = '' ;

	Load_Script::getInstance()->enqueue_script("js/confirm_modif_user.js");

	$this->vue = ROOT_PATH .'views/updateUserView.php';	
}