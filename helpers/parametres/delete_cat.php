<?php
$validation = new Data_Validation();

if( ! $validation->verifNumber( $_POST['id_delete'] ) ){

	$this->alertModifSuppCategorie = 'Non mais tu t\'es cru où là?' ;

	Load_Script::getInstance()->enqueue_script("js/confirm_supp_cat.js");

	$this->vue = ROOT_PATH .'views/listeCategoriesView.php';
}
else{

	$d = new categoriesModel();
	$id_delete = $_POST['id_delete'];
	$d->delete_cat($id_delete);
	$this->data = $d->get_category();
	$this->cats = get_cats($this->data);
	$this->alertModifSuppCategorie = "Catégorie supprimée";

	Load_Script::getInstance()->enqueue_script("js/confirm_supp_cat.js");
	
	$this->vue = ROOT_PATH .'views/listeCategoriesView.php';
}