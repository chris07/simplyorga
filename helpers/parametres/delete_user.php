<?php
$validation = new Data_Validation();

if( ! $validation->verifNumber( $_POST['id_delete'] ) ){

	$this->alertModifSuppUser = 'Non mais tu t\'es cru où là?' ;

}
else{

	$d = new usersModel();
	$id_delete = $_POST['id_delete'];
	$d->delete_user($id_delete);
	$this->data = $d->get_users();
	$this->alertModifSuppUser = 'Utilisateur Supprimmé avec succès!';
}