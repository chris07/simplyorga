<?php
	$d = new categoriesModel();
	$validation = new Data_Validation();
	$id_update = $_POST['id'];
    $nom = $_POST['nom'];
    if($_POST['parent_cat'] === 'evenementiel'){$cat = 2;}else{$cat = 1;}
    $this->data = $d->get_cat($id_update);
    
    if( ! $validation->verifTextNumber( $id_update ) ){

    	$d = new categoriesModel();
		$this->data = $d->get_category();
		$this->cats = get_cats($this->data);
		$this->alertModifSuppCategorie = 'Non mais tu t\'es cru où là?' ;

		Load_Script::getInstance()->enqueue_script("js/confirm_supp_cat.js");

		$this->vue = ROOT_PATH .'views/listeCategoriesView.php';
	}
	elseif(! $validation->verifTextNumber( $nom )){

		$d = new categoriesModel();
		$this->data = $d->get_cat($id_update);
		
		$this->alertModifSuppCategorie = 'Le nom ne peut contenir que des chiffres et des lettres!' ;

		Load_Script::getInstance()->enqueue_script("js/confirm_modif_cat.js");

		$this->vue = ROOT_PATH .'views/updateCatView.php';	
	}
	elseif(! $validation->verifNumber( $cat )){

		$d = new categoriesModel();
		$this->data = $d->get_category();
		$this->cats = get_cats($this->data);
		$this->alertModifSuppCategorie = 'faut pas test!' ;

		Load_Script::getInstance()->enqueue_script("js/confirm_supp_cat.js");

		$this->vue = ROOT_PATH .'views/listeCategoriesView.php';
	}
	else{

		$d = new categoriesModel();
		$d->update_cat($id_update, $nom, $cat);
		$this->data = $d->get_category();
		$this->cats = get_cats($this->data);
		$this->alertModifSuppCategorie = 'Catégorie modifiée avec succès!' ;

		Load_Script::getInstance()->enqueue_script("js/confirm_supp_cat.js");

		$this->vue = ROOT_PATH .'views/listeCategoriesView.php';
	}
