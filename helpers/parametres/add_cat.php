<?php
$validation = new Data_Validation();
$nom = $_POST['nom'];
if(isset( $_POST['parent_cat'])){

	$parent_cat = $_POST['parent_cat'];
}
else{

	$parent_cat = null;
}


if( ! $validation->verifText( $nom ) &&  ! $validation->verifText( $parent_cat )){

	$d = new categoriesModel();
	$this->data = $d->get_category();
	$this->cats = get_cats($this->data);
	$this->alertAjoutCat = 'Le nom ou la catégorie parente ne sont pas renseignés!' ;

	$this->vue = ROOT_PATH .'views/ajoutCategorieView.php';
}
else{

	if ($parent_cat === "materiel"){

		$d = new categoriesModel();
		$id_cat = 1;
		$d->register_cat($nom, $id_cat);
		$this->data = $d->get_category();
		$this->alertModifSuppCategorie = "Catégorie enregistrée";
		$this->cats = get_cats($this->data);

		$this->vue = ROOT_PATH .'views/listeCategoriesView.php';
	}
	elseif($parent_cat === "evenementiel"){

		$d = new categoriesModel();
		$id_cat = 2;
		$d->register_cat($nom, $id_cat);
		$this->data = $d->get_category();
		$this->cats = get_cats($this->data);
		$this->alertModifSuppCategorie = "Catégorie enregistrée";

		$this->vue = ROOT_PATH .'views/listeCategoriesView.php';
	}
	else{

		$d = new categoriesModel();
		$this->data = $d->get_category();
		$this->cats = get_cats($this->data);
		$this->alertAjoutCat = 'La catégorie parente doit être renseignés!' ;

		$this->vue = ROOT_PATH .'views/ajoutCategorieView.php';
	}
}