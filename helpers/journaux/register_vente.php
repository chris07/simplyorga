<?php
$validation = new Data_Validation();

if(isset($_POST['statut'])){

	$statut = $_POST['statut'];
}
else{

	$statut = null;
}
$an =$_POST['an'];
$mois = $_POST['mois'];
$nombre = $_POST['nombre'];
$montant = $_POST['montant'];
$obj_nombre = $_POST['objectif_nombre'];
$obj_montant = $_POST['objectif_montant'];


//si c'est une vente 
if( $statut === 'vente' ){	

	
	// si période pas valide
	if(! $validation->verifNumber( $an ) || ! $validation->verifNumber( $mois )){

		$this->alertVentes = 'La période n\'est pas valide.';

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$this->vue = ROOT_PATH .'views/ventesJournauxView.php';
	}
	//sinon si nombre ou montant pas valide
	elseif(! $validation->verifNumber( $nombre ) || ! $validation->verifFloat( $montant )){

		$this->alertVentes = 'Le nombre ou le montant ne sont pas valides.';

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$this->vue = ROOT_PATH .'views/ventesJournauxView.php';
	}
	//sinon on insert ou update
	else{

		$d =new journauxModel();


		$periode_exist = $d->periode_exist($an, $mois);
		//si la période existe
		if( $periode_exist === true ){

			$d =new journauxModel();
			$d->update_vente($an, $mois, $nombre, $montant);
			$this->alertVentes = 'La vente a bien été modifiée.';

			Load_Script::getInstance()->enqueue_script("js/show_alert.js");

			$this->vue = ROOT_PATH .'views/indexJournauxView.php';
		}
		//sinon
		else{

			$d =new journauxModel();
			$d->insert_vente($an, $mois, $nombre, $montant);
			$this->alertVentes = 'La vente a bien été enregistrée.';

			Load_Script::getInstance()->enqueue_script("js/show_alert.js");

			$this->vue = ROOT_PATH .'views/indexJournauxView.php';
		}
	}
}
//sinon si c'est un objectif
elseif( $statut === 'objectif' ){

	
	// si période pas valide
	if(! $validation->verifNumber( $an ) || ! $validation->verifNumber( $mois )){

		$this->alertVentes = 'La période n\'est pas valide.';

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$this->vue = ROOT_PATH .'views/ventesJournauxView.php';
	}
	//sinon si nombre ou montant pas valide
	elseif(! $validation->verifNumber( $obj_nombre ) || ! $validation->verifFloat( $obj_montant )){
		
		$this->alertVentes = 'Le nombre ou le montant de l\'objectif ne sont pas valides.';

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$this->vue = ROOT_PATH .'views/ventesJournauxView.php';
	}
	//sinon on insert ou update
	else{

		$d =new journauxModel();
		$periode_exist = $d->periode_exist($an, $mois);
		//si la période existe
		if($periode_exist === true){

			$d->update_obj($an, $mois, $obj_nombre, $obj_montant);
			$this->alertVentes = 'L\'objectif a bien été modifiée.';

			Load_Script::getInstance()->enqueue_script("js/show_alert.js");

			$this->vue = ROOT_PATH .'views/indexJournauxView.php';
		}
		//sinon
		else{

			$d->insert_obj($an, $mois, $obj_nombre, $obj_montant);
			$this->alertVentes = 'L\'objectif a bien été enregistrée.';

			Load_Script::getInstance()->enqueue_script("js/show_alert.js");

			$this->vue = ROOT_PATH .'views/indexJournauxView.php';
		}
	}
}
//sinon c'est une error
else{

	//si statut pas valide
	if(! $validation->verifText( $statut )){

		$this->alertVentes = 'Le statut n\'est pas valide.';

		$this->vue = ROOT_PATH .'views/ventesJournauxView.php';
	}
	else{

		$this->vue = ROOT_PATH .'controllers/errorController.php';
	}
}