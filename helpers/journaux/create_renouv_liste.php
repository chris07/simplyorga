<?php

function create_renouv_list($adhs){

	$dateJour = date('Y-m-d');
	$ids = array();
	$noms = array();
	$prenoms = array();
	$emails = array();
	$fins = array();
	$results = array();

	foreach ($adhs as $adh ) {

		if($adh['abonnement'] === "1"){

			$date_fin = $adh['date_fin'];
			$dfin = explode("-", $date_fin); 
			$finab = intval($dfin[0].$dfin[1].$dfin[2]); 
			$djour = explode("-", $dateJour); 
			$auj = intval($djour[0].$djour[1].$djour[2]); 
			
			if ($auj > $finab && $finab != 0){

				array_push($ids, $adh['id']);
				array_push($noms, $adh['nom']); 
				array_push($prenoms, $adh['prenom']);
				array_push($emails, $adh['mail']);
				array_push($fins, $adh['date_fin']);
			}
		}
	}

	foreach ($ids as $adhexp => $key) {

	    $results[$key] = array(
	    	'id' => $ids[$adhexp],
	        'nom'  => $noms[$adhexp],
	        'prenom' => $prenoms[$adhexp],
	        'e-mail'    => $emails[$adhexp],
		        'date_fin'    => $fins[$adhexp],
    	);
	}

	return $results;

}