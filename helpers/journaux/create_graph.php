<?php
require_once ROOT_PATH .'app/ChartsJs.php';


function create_graph($data1, $data2, $data3,$year){

	$nom = 'Bilan '.$year;
	$colors1 = array('rgba(25,255,64,0.7)', 'rgba(0,128,255,0.7)');
	$colors2 = array('rgba(230,38,0,0.7)', 'rgba(255,213,0,0.7)');
	$xLabel = array(
		'Janvier Février',
		'Mars Avril',
		'Mai Juin',
		'Juillet Août',
		'Septembre Octobre',
		'Novembre Décembre'
	);
	$dataset = array(
		'objectif',
		'vendu',
		'montant objectif',
		'montant vendu'
	);
	$dataset2 = array(
		'nombre objectif',
		'nombre vendu'
	);
	$dataset3 = array(
		'montant objectif',
		'montant vendu'
	);
	$alertbilan = "";


		
	$graph = new Chart_Manager();

	$bargraph =  $graph->barChart($nom, $xLabel, $dataset, $data1['tableau graph1']);
	$piegraph =  $graph->pieChart('canvas-pie' , $dataset2, $data2, $colors1);
	$piegraph2 =  $graph->pieChart('canvas-pie2',$dataset3, $data3, $colors2);
	$linegraph = $graph->lineChart($xLabel, $dataset, $data1['tableau graph1']);

	$value_return = array($bargraph,$piegraph,$piegraph2,$linegraph);

	return $value_return;
	
}