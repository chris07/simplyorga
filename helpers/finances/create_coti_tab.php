<?php

require_once ROOT_PATH .'models/cotisationsModel.php';

function create_coti_tab($users, $year){

	$c =new cotisationsModel();
	$usersID = array();

	foreach ($users as $user) {
		
		array_push($usersID, $user['id']);
	}

	$data = $c->get_all_coti($usersID, $year);

	$dataliste = array();

	foreach($data as $coti){

		$annee = array(
			"janvier" => 0,
			"fevrier" => 0,
			"mars" => 0,
			"avril" => 0,
			"mai" => 0,
			"juin" => 0,
			"juillet" => 0,
			"aout" => 0,
			"septembre" => 0,
			"octobre" => 0,
			"novembre" => 0,
			"decembre" => 0
		);

		$janvier = 0;
		$fevrier = 0;
		$mars = 0;
		$avril = 0;
		$mai = 0;
		$juin = 0;
		$juillet = 0;
		$aout = 0;
		$septembre = 0;
		$octobre = 0;
		$novembre = 0;
		$decembre = 0;

		foreach ($coti as $line){

			if(isset($line['mois'])  && $line['mois'] == "0"){

				$janvier = $line['montant'];
			}
			elseif(isset($line['mois'])  && $line['mois'] == "1"){

				$fevrier = $line['montant'];
			}
			elseif(isset($line['mois'])  && $line['mois'] == "2"){

				$mars = $line['montant'];
			}
			elseif(isset($line['mois'])  && $line['mois'] == "3"){

				$avril = $line['montant'];
			}
			elseif(isset($line['mois'])  && $line['mois'] == "4"){

				$mai = $line['montant'];
			}
			elseif(isset($line['mois'])  && $line['mois'] == "5"){

				$juin = $line['montant'];
			}
			elseif(isset($line['mois'])  && $line['mois'] == "6"){

				$juillet = $line['montant'];
			}
			elseif(isset($line['mois'])  && $line['mois'] == "7"){

				$aout = $line['montant'];
			}
			elseif( isset($line['mois'])  && $line['mois'] == "8"){

				$septembre = $line['montant'];
			}
			elseif(isset($line['mois'])  && $line['mois'] == "9"){

				$octobre = $line['montant'];
			}
			elseif(isset($line['mois'])  && $line['mois'] == "10"){

				$novembre = $line['montant'];
			}
			elseif(isset($line['mois'])  && $line['mois'] == "11"){
				
				$decembre = $line['montant'];
			}
			else{
				
			}
		}

		$annee['janvier'] = $janvier;
		$annee['fevrier'] = $fevrier;
		$annee['mars'] = $mars;
		$annee['avril'] = $avril;
		$annee['mai'] = $mai;
		$annee['juin'] = $juin;
		$annee['juillet'] = $juillet;
		$annee['aout'] = $aout;
		$annee['septembre'] = $septembre;
		$annee['octobre'] = $octobre;
		$annee['novembre'] = $novembre;
		$annee['decembre'] = $decembre;

		array_push($dataliste,$annee);
	}

	return $dataliste;
}