<?php

require_once ROOT_PATH .'config/config.php';
require_once ROOT_PATH .'app/Model.php';

class journauxModel extends Model
{
    private $db_manager;
    
    function __construct(){

        $this->db_manager = new Model(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);
    }

    /**
    *Vérifie si la période existe déjà en base de donnée
    *
    *@param int $an
    *@param int $mois
    *@return bool $value_return true si elle existe false sinon
    */
    public function periode_exist($an, $mois){
        

        $value_return = false;

        $arg = array(
            "type" => "SELECT",
            "table" => "vente_journaux",
            "fields" => array(
                'an',
                'semestre'           
            ),
            "condition" => "an = ? AND semestre = ?",
            "value-condition" => array(
                $an,
                $mois
            ),
        );

        $res = $this->db_manager->query( $arg );

        if(!empty($res)){

            $value_return = true;
        }

        return $value_return;
    }


    /**
    *Insert une vente en bdd
    *
    *@param int $an
    *@param int $mois
    *@param int $nombre
    *@param float $montant
    *@return bool $value_return true si ok false sinon
    */
    public function insert_vente($an, $mois, $nombre, $montant){
        

        $value_return = true;
        
        $arg = array(
            "type" => "INSERT",
            "table" => "vente_journaux",
            "fields" => array(
                'an',
                'semestre',
                'vendu',
                'montant'
            ),
            "values" => array(
                $an, 
                $mois, 
                $nombre, 
                $montant
            ),
        );
       
        if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

        return $value_return;
    }



    /**
    *Modifie une vente en bdd
    *
    *@param int $an
    *@param int $mois
    *@param int $nombre
    *@param float $montant
    *@return bool $value_return true si ok false sinon
    */
    public function update_vente($an, $mois, $nombre, $montant){
        

        $value_return = true;
        
        $arg = array(
            "type" => "UPDATE",
            "table" => "vente_journaux",
            "fields" => array(
                'vendu',
                'montant'
            ),
            "values" => array(
                $nombre, 
                $montant
            ),
            "condition" => "an = ? AND semestre = ?",
            "value-condition" => array(
                $an,
                $mois
            ),
        );
       
        if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

        return $value_return;
    }


    /**
    *Insert un objectif en bdd
    *
    *@param int $an
    *@param int $mois
    *@param int $nombre
    *@param float $montant
    *@return bool $value_return true si ok false sinon
    */
    public function insert_obj($an, $mois, $nombre, $montant){
        

        $value_return = true;
        
        $arg = array(
            "type" => "INSERT",
            "table" => "vente_journaux",
            "fields" => array(
                'an',
                'semestre',
                'objectif',
                'objectif_montant'
            ),
            "values" => array(
                $an, 
                $mois, 
                $nombre, 
                $montant
            ),
        );
       
        if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

        return $value_return;
    }

    /**
    *Modifie un objectif en bdd
    *
    *@param int $an
    *@param int $mois
    *@param int $nombre
    *@param float $montant
    *@return bool $value_return true si ok false sinon
    */
    public function update_obj($an, $mois, $nombre, $montant){
        

        $value_return = true;
        
        $arg = array(
            "type" => "UPDATE",
            "table" => "vente_journaux",
            "fields" => array(
                'objectif',
                'objectif_montant'
            ),
            "values" => array(
                $nombre, 
                $montant
            ),
            "condition" => "an = ? AND semestre = ?",
            "value-condition" => array(
                $an,
                $mois
            ),
        );
       
        if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

        return $value_return;
    }


    public function get_bilan_by_year($year){
        

        $value_return = false;

        $arg = array(
            "type" => "SELECT",
            "table" => "vente_journaux",
            "fields" => array(
                'semestre',
                'montant',
                'objectif',
                'objectif_montant',
                'vendu'          
            ),
            "condition" => "an = ?",
            "value-condition" => array(
                $year
            ),
        );

        $data = $this->db_manager->query( $arg );

        $tab = array(
            0 =>    array(  "semestre"  => 1,
                            "montant" => 0  ,
                            "objectif" => 0 ,
                            "objectif_montant"=> 0  ,
                            "vendu"=> 0
                    ),
            1 =>    array(  "semestre" => 2 ,
                            "montant"=> 0   ,
                            "objectif"=> 0  ,
                            "objectif_montant"=> 0  ,
                            "vendu"=> 0 
                    ),
            2 =>    array(  "semestre" => 3 ,
                            "montant"=> 0   ,
                            "objectif"=> 0  ,
                            "objectif_montant"=> 0  ,
                            "vendu" => 0
                    ),
            3 =>    array(  "semestre"=> 4  ,
                            "montant" => 0  ,
                            "objectif" => 0 ,
                            "objectif_montant" => 0 ,
                            "vendu" => 0    
                    ),
            4 =>    array(  "semestre" => 5 ,
                            "montant" => 0  ,
                            "objectif" => 0 ,
                            "objectif_montant" => 0 ,
                            "vendu" => 0
                    ),  
            5 =>    array(  "semestre" => 6 ,
                            "montant" => 0  ,
                            "objectif" => 0 ,
                            "objectif_montant" => 0 ,
                            "vendu" => 0
                    ),  
        );

        
        $nombrevendu = 0 ;
        $objvente = 0;
        $montobjectif = 0 ;
        $montvente = 0 ;


        foreach ($data as $dat) {

            $sem = $dat['semestre'];

            for($i=1; $i <= 6; $i++) {
                if($sem == $i){

                    $tab[$i-1]["montant"] = $dat['montant'];
                    $tab[$i-1]["objectif"] = $dat['objectif'];
                    $tab[$i-1]["objectif_montant"] = $dat['objectif_montant'];
                    $tab[$i-1]["vendu"] = $dat['vendu'];

                    $nombrevendu +=  $tab[$i-1]["vendu"];
                    $objvente += $tab[$i-1]["objectif"];
                    $montobjectif += $tab[$i-1]["objectif_montant"];
                    $montvente += $tab[$i-1]["montant"];
                }
            }
        }

        $value_return = array(  'tableau graph1' => $tab,
                                'nombrevendu' => $nombrevendu,
                                'objectif_nombre' => $objvente,
                                'montant_obj' => $montobjectif,
                                'montant_vendu' => $montvente) ;

        return $value_return ;
    }


    public function update_abonnement($id, $date_new_abonn, $dateFin){
        

        $value_return = true;
        
        $arg = array(
            "type" => "UPDATE",
            "table" => "adherants",
            "fields" => array(
                "date_abonnement",
                "date_fin"
            ),
            "values" => array(
                $date_new_abonn, 
                $dateFin
            ),
            "condition" => "id = ?",
            "value-condition" => array($id)
        );
       
        if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

        return $value_return;
    }

}


