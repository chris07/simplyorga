<?php

require_once ROOT_PATH .'config/config.php';
require_once ROOT_PATH .'app/Model.php';



/**
 * 
 */
class usersModel extends Model
{
    private $db_manager;
    
    function __construct(){

        $this->db_manager = new Model(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);
    }

    /**
    * Retourne l'ID de la personne qui veut se connecter, false si les identifiants
    * ne correspondent pas
    * 
    * @param string $login
    * @param string $password
    * @return bool | int $value_return L'id du user, false s'il la connexion a échoué
    */
    public function is_connecting( $login, $password ) {
        
    
        $arg = array(
            "type" => "SELECT",
            "table" => "users",
            "fields" => array(
                'id',
                'password'
            ),
            "condition" => "login = ?",
            "value-condition" => array($login),
        );
    
        $res = $this->db_manager->query( $arg );

    
        $value_return = false;

        if ( password_verify( $password, $res[0]['password'])){
        
            $value_return = intval($res[0]['id']);
        } 

        return $value_return; 
    }



    /**
    *Ajoute un utilisateur en base de donnée
    *
    *@param string $nom
    *@param string $prenom
    *@param string $login
    *@param string $password
    *@return bool true si ok false sinon
    */
    public function add_user( $nom, $prenom, $login, $password ){
    

        $value_return = true;
    
        $arg = array(
            "type" => "INSERT",
            "table" => "users",
            "fields" => array(
                'nom',
                'prenom',
                'login',
                'password'
            ),
            "values" => array(
                $nom,
                $prenom,
                $login,
                $password,
            ),
        );
   
        if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

        return $value_return;
    }



    /**
    *Retourne le prénom de l'utilisateur en fonction de son id
    *
    *@param int $id_user
    *@return string $value_return Prénom de l'utilisateur
    */
    public function get_prenom_user($id_user){
    

        $arg = array(
            "type" => "SELECT",
            "table" => "users",
            "fields" => array(
                'prenom'
            ),
            "condition" => "id = ?",
            "value-condition" => array($id_user),
        );
    
        $res = $this->db_manager->query( $arg );
        
        $value_return = $res[0]['prenom'] ;

        return $value_return;
    }

    /**
    *Retourne les infos des utilisateurs
    *
    *@return array $res
    */
    public function get_users(){
    

        $arg = array(
            "type" => "SELECT",
            "table" => "users",
            "fields" => array(
                'id',
                'nom',
                'prenom',
                'login',
            ),
        );

        $res = $this->db_manager->query( $arg );

        return  $res ;   
    }



    /**
    *Retourne les infos d'un utilisateur
    *
    *@param int $id 
    *@return array() $res données de l'utilisateur
    */
    public function get_user($id){
   

        $arg = array(
            "type" => "SELECT",
            "table" => "users",
            "fields" => array(
                'id',
                'nom',
                'prenom',
                'login',
            ),
            "condition" => "id = ?",
            "value-condition" => array($id),
        );

        $res = $this->db_manager->query( $arg );

        return  $res ;   

    }


    /**
    *Efface un utilisateur
    *
    *@param int $id
    *@return bool true si ok
    */
    public function delete_user ($id){
    
    
        $arg = array (
            "type" => "DELETE",
            "table" => "users",
            "condition" => "id = ?",
            "value-condition" => array ($id),
        );
    
        return ($this->db_manager->query($arg));
    }


    /**
    *Retourne tous les logins des utilisateurs
    *
    *@return array() $res
    */
    public function get_logins(){
    

        $arg = array(
            "type" => "SELECT",
            "table" => "users",
            "fields" => array(
                'login',
            ),
        );

        $res = $this->db_manager->query( $arg );

        $value_return = array();
        
            foreach( $res as $single ) {
            
                $value_return[ $single[0] ] = $res['login'] ;
            }

        return  $value_return ;   
    }


    /**
    *Modifie un utilisateur
    *
    *@param string $nom
    *@param string $prenom
    *@param string $login
    *@param string $password
    *@return bool true si ok false sinon
    */
    public function update_user($id, $nom, $prenom, $login){
    

        $value_return = true;
    
        $arg = array(
            "type" => "UPDATE",
            "table" => "users",
            "fields" => array(
                'nom',
                'prenom',
                'login',
            ),
            "values" => array(
                $nom,
                $prenom,
                $login,
            ),
            "condition" => "id = ?",
            "value-condition" => array($id),
        );
   
        if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

        return $value_return;
    }



    public function admin($id){


        $value_return = true;

        $arg = array(
            "type" => "SELECT",
            "table" => "users",
            "fields" => array(
                'admin',
            ),
            "condition" => "id = ?",
            "value-condition" => array($id),
        );


        $res = $this->db_manager->query( $arg );

        $value_return = intval($res[0]['admin']);


        return $value_return;
    }

}



