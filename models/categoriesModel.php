<?php

require_once ROOT_PATH .'config/config.php';
require_once ROOT_PATH .'app/Model.php';


/**
 * 
 */
class categoriesModel extends Model
{

	private $db_manager;
	
	function __construct(){

		$this->db_manager = new Model(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);
	}


	/**
	*Retourne les infos les categories
	*
	*@global DB_manager $db_manager
	*@return array $res
	*/
	public function get_category(){
	    

	    $arg = array(
	        "type" => "SELECT",
	        "table" => "sous_categories",
	        "fields" => array(
	            'id',
	            'id_cat',
	            'nom'
	        ),
	    );

	    $res = $this->db_manager->query( $arg );

	    return  $res ;   
	}

	public function get_cat($id){
		

		$arg = array(
    	    "type" => "SELECT",
    	    "table" => "sous_categories",
    	    "fields" => array(
    	        'id',
    	        'nom',
    	        'id_cat'
    	    ),
    	    "condition" => "id = ?",
    	    "value-condition" => array($id),
    	);

    	$res = $this->db_manager->query( $arg );

    	return  $res ;   
	}

	public function get_cat_name($id){
		

		$arg = array(
    	    "type" => "SELECT",
    	    "table" => "sous_categories",
    	    "fields" => 'nom',
    	    "condition" => "id = ?",
    	    "value-condition" => array($id),
    	);

    	$res = $this->db_manager->query( $arg );


    	return  $res[0]['nom'] ;   
	}

	public function register_cat($nom, $parent_cat){
		

		$value_return = true;
    
    	$arg = array(
    	    "type" => "INSERT",
    	    "table" => "sous_categories",
    	    "fields" => array(
    	        "nom",
    	        "id_cat"
    	    ),
    	    "values" => array(
    	        $nom,
    	        $parent_cat
    	    ),
    	);
   
    	if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

    	return $value_return;
	}



	public function update_cat($id_update, $nom, $cat){
		

		$value_return = true;
    
	    $arg = array(
	        "type" => "UPDATE",
	        "table" => "sous_categories",
	        "fields" => array(
	            'nom',
	            'id_cat'
	        ),
	        "values" => array(
	            $nom,
	            $cat,
	        ),
	        "condition" => "id = ?",
	        "value-condition" => array($id_update),
	    );
   
	    if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

	    return $value_return;
	}

	public function delete_cat($id_delete){
	

		$arg = array (
	        "type" => "DELETE",
	        "table" => "sous_categories",
	        "condition" => "id = ?",
	        "value-condition" => array ($id_delete),
	    );
    
   	 	return ($this->db_manager->query($arg));
	}


	public function get_cat_by_name($name){

		$arg = array (
	        "type" => "SELECT",
	        "table" => "sous_categories",
	        "fields" => "id",
	        "condition" => "nom = ?",
	        "value-condition" => array ($name),
	    );
    
   	 	return ($this->db_manager->query($arg));


	}

}






