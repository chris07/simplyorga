<?php

require_once ROOT_PATH .'config/config.php';
require_once ROOT_PATH .'app/Model.php';



class cotisationsModel extends Model
{
    private $db_manager;
    
    function __construct(){

        $this->db_manager = new Model(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);
    }

    public function get_coti_by_adh($adh, $an){
        

        $arg = array(
            "type" => "SELECT",
            "table" => "cotisations",
            "fields" => array(
                'mois',
                'montant',
            ),
            "condition" => "id_adh = ? AND an = ?",
            "value-condition" => array($adh, $an)
        );

        $res = $this->db_manager->query( $arg );

        return  $res ;   
    }

    public function coti_exist($adh,$year,$mois){
        

        $value_return = false;
        
        $arg = array(
            "type" => "SELECT",
            "table" => "cotisations",
            "fields" => array(
                'montant',
            ),
            "condition" => "id_adh = ? AND an = ? AND mois = ?",
            "value-condition" => array($adh, $year, $mois)
        );

        $res = $this->db_manager->query( $arg );

        if(!empty($res)){

            $value_return = true;
        }
        
        return  $value_return ;   

    }

    public function update_coti_adh($adh, $year, $mois, $value){
        

        $value_return = true;
        
        $arg = array(
            "type" => "UPDATE",
            "table" => "cotisations",
            "fields" => array(
                "montant"
            ),
            "values" => array(
                $value, 
               
            ),
            "condition" => "id_adh = ? AND an = ? AND mois = ?",
            "value-condition" => array(
                $adh,
                $year,
                $mois
            ),
        );
       
        if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

        return $value_return;

    }

    public function insert_coti_adh($adh, $year, $mois, $value){
        

        $value_return = true;
        
        $arg = array(
            "type" => "INSERT",
            "table" => "cotisations",
            "fields" => array(
                'id_adh',
                'an',
                'mois',
                'montant'
            ),
            "values" => array(
                $adh,
                $year,
                $mois, 
                $value
            ),
        );
       
        if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

        return $value_return;
    }

    public function get_all_coti($adhs, $year){

        $data = array();

        foreach ($adhs as $adh => $value) {
            
            $arg = array(
                "type" => "SELECT",
                "table" => "cotisations",
                "fields" => array(
                    'id_adh',
                    'an',
                    'mois',
                    'montant'
                ),
                "condition" => "id_adh = ? AND an = ?",
                "value-condition" => array($value, $year)
            );

            $res = $this->db_manager->query( $arg );

            array_push($data, $res);
        }

        return $data;
    }
}

