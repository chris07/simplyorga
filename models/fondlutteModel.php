<?php

require_once ROOT_PATH .'config/config.php';
require_once ROOT_PATH .'app/Model.php';


class fondlutteModel extends Model
{
    private $db_manager;
    
    function __construct(){

        $this->db_manager = new Model(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);
    }

    public function get_fond_infos(){
        

        $arg = array(
            "type" => "SELECT",
            "table" => "fond_de_lutte",
            "fields" => array(
                '*'
            ),
            "order" => "id_transac DESC",
            "limit" => 200
        );

        $res = $this->db_manager->query( $arg );

        return  $res ;   
    }

    public function add_transac($type, $cat, $montant, $date, $comm, $solde){

        $val_return = false;

        $arg = array(
            "type" => "INSERT",
            "table" => "fond_de_lutte",
            "fields" => array(
                'type',
                'id_sous_cat',
                'montant',
                'date',
                'commentaire',
                'solde'
            ),
            "values" => array(
               $type, 
               $cat, 
               $montant, 
               $date, 
               $comm, 
               $solde 
            ) 
        );


        if($res = $this->db_manager->query( $arg )){

            $val_return = true;
        }

        return  $val_return ;

    }
}
