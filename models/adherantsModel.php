<?php

require_once ROOT_PATH .'config/config.php';
require_once ROOT_PATH .'app/Model.php';



/**
 * 
 */
class adherantsModel extends Model
{
    private $db_manager;
    
    function __construct(){

        $this->db_manager = new Model(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);
    }

    /**
    *Ajoute un adhérant en base de donnée
    *
    *@param string $nom
    *@param string $prenom
    *@param string $adresse
    *@param int $tel
    *@param string $mail
    *@param bool $abonnement
    *@param date $date
    *@param string $coti
    *@return bool true si ok false sinon
    */
    public function add_adh( $nom, $prenom, $adresse, $tel, $mail, $abonnement, $date, $dateFin, $cotisation, $coti ){
        

        $value_return = true;
    
        $arg = array(
            "type" => "INSERT",
            "table" => "adherants",
            "fields" => array(
                'nom',
                'prenom',
                'adresse_postale',
                'telephone',
                'mail',
                'abonnement',
                'date_abonnement',
                'date_fin',
                'cotisation',
                'mode_paiement_coti'
            ),
            "values" => array(
                $nom,
                $prenom,
                $adresse, 
                $tel, 
                $mail, 
                $abonnement, 
                $date, 
                $dateFin,
                $cotisation,
                $coti
            ),
        );
   
        if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

         return $value_return;
    }

    /**
    *Retourne les infos des adhérants
    *
    *@return array $res
    */
    public function get_adherants(){
        

        $arg = array(
            "type" => "SELECT",
            "table" => "adherants",
            "fields" => array(
                'id',
                'nom',
                'prenom',
                'adresse_postale',
                'telephone',
                'mail',
                'abonnement',
                'date_abonnement',
                'date_fin',
                'mode_paiement_coti'
            ),
        );

        $res = $this->db_manager->query( $arg );

        return  $res ;   
    }


    public function get_adh($id){
        

        $arg = array(
            "type" => "SELECT",
            "table" => "adherants",
            "fields" => array(
                'id',
                'nom',
                'prenom',
                'adresse_postale',
                'telephone',
                'mail',
                'abonnement',
                'date_abonnement',
                'date_fin',
                'cotisation',
                'mode_paiement_coti'
            ),
            "condition" => "id = ?",
            "value-condition" => array($id),
        );

        $res = $this->db_manager->query( $arg );

        return  $res ;   
    }


    public function update_adh($nom, $prenom, $adresse, $tel, $mail, $abonnement, $date, $dateFin, $cotisation, $coti, $id){
        

        $value_return = true;
    
        $arg = array(
            "type" => "UPDATE",
            "table" => "adherants",
            "fields" => array(
                'nom',
                'prenom',
                'adresse_postale',
                'telephone',
                'mail',
                'abonnement',
                'date_abonnement',
                'date_fin',
                'cotisation',
                'mode_paiement_coti'
            ),
            "values" => array(
                $nom, 
                $prenom, 
                $adresse, 
                $tel, 
                $mail, 
                $abonnement, 
                $date, 
                $dateFin,
                $cotisation,
                $coti
            ),
            "condition" => "id = ?",
            "value-condition" => array($id),
        );
   
        if ( ! $this->db_manager->query( $arg ) ) $value_return = false;

        return $value_return;
    }


    public function delete_adh ($id){
        
    
        $arg = array (
            "type" => "DELETE",
            "table" => "adherants",
            "condition" => "id = ?",
            "value-condition" => array ($id),
        );
    
        return ($this->db_manager->query($arg));
    }

    public function get_adh_non_virements(){
        

        $arg = array(
            "type" => "SELECT",
            "table" => "adherants",
            "fields" => array(
                'id',
                'nom',
                'prenom',
                'adresse_postale',
                'telephone',
                'mail',
                'abonnement',
                'date_abonnement',
                'date_fin',
            ),
            "condition" => "mode_paiement_coti != ? AND cotisation = 1",
            "value-condition" => array("'virement'")
        );

        $res = $this->db_manager->query( $arg );

        return  $res ;   
    }

}
