<?php


// array key = url name and title, array value = file name
$page = array(
    
    "parametres" => array(
        'file-name' => "parametreController.php",
        'visibility' => "private",
        'title' => 'Paramètres',
        'menu' => true,
    ),
    "ajout_user" => array(
        'file-name' => "parametreController.php",
        'visibility' => "private",
        'title' => 'Ajout Utilisateur',
        'menu' => false,
    ),
    "modif_user" => array(
        'file-name' => "parametreController.php",
        'visibility' => "private",
        'title' => 'Modifier Utilisateur',
        'menu' => false,
    ),
    "liste_users" => array(
        'file-name' => "parametreController.php",
        'visibility' => "private",
        'title' => 'Liste des Utilisateurs',
        'menu' => false,
    ),
    "liste_categories" => array(
        'file-name' => "parametreController.php",
        'visibility' => "private",
        'title' => 'Liste des Catégories',
        'menu' => false,
    ),
    "ajout_cat" => array(
        'file-name' => "parametreController.php",
        'visibility' => "private",
        'title' => 'Ajout Catégorie',
        'menu' => false,
    ),
    "adherants" => array(
        'file-name' => "adherantsController.php",
        'visibility' => "private",
        'title' => 'Adhérants',
        'menu' => true,
    ),
    "ajout_adh" => array(
        'file-name' => "adherantsController.php",
        'visibility' => "private",
        'title' => 'Nouvel Adhérant',
        'menu' => false,
    ),
    "modif_adh" => array(
        'file-name' => "adherantsController.php",
        'visibility' => "private",
        'title' => 'Modifier Adhérant',
        'menu' => false,
    ),
    "liste_adh" => array(
        'file-name' => "adherantsController.php",
        'visibility' => "private",
        'title' => 'Liste des Adhérants',
        'menu' => false,
    ),
    "Journaux" => array(
        'file-name' => "journauxController.php",
        'visibility' => "private",
        'title' => 'Journaux',
        'menu' => true,
    ),
    "ventes" => array(
        'file-name' => "journauxController.php",
        'visibility' => "private",
        'title' => 'Ventes Journaux',
        'menu' => false,
    ),
    "bilans" => array(
        'file-name' => "journauxController.php",
        'visibility' => "private",
        'title' => 'Bilans Journaux',
        'menu' => false,
    ),
    "abonnements" => array(
        'file-name' => "journauxController.php",
        'visibility' => "private",
        'title' => 'Abonnements Journaux',
        'menu' => false,
    ),
    "finances" => array(
        'file-name' => "financesController.php",
        'visibility' => "private",
        'title' => 'Finances',
        'menu' => true,
    ),
    "liste_coti" => array(
        'file-name' => "financesController.php",
        'visibility' => "private",
        'title' => 'Cotisations',
        'menu' => false,
    ),
    "saisie_cotisations" => array(
        'file-name' => "financesController.php",
        'visibility' => "private",
        'title' => 'Cotisations',
        'menu' => false,
    ),
    "fond_lutte" => array(
        'file-name' => "financesController.php",
        'visibility' => "private",
        'title' => 'Fond de Lutte',
        'menu' => false,
    ),
    "connexion" => array(
        'file-name' => "connexionController.php",
        'visibility' => "public",
        'title' => 'Connexion',
        'menu' => true,
    ),
    "deconnexion" => array(
        'file-name' => "deconnexionController.php",
        'visibility' => "private",
        'title' => 'Déconnexion',
        'menu' => true,
    )
);  
