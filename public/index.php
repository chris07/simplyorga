<?php


define('ROOT_PATH', dirname(__FILE__ , 2).'/');

require_once  ROOT_PATH .'config/urls_pages.php';
require_once  ROOT_PATH .'app/Data_Validation.php';
require_once  ROOT_PATH .'app/Data_Validation.php';


session_start();


ob_start();


if(isset($_GET['p']) && !empty($_GET['p']) && array_key_exists($_GET['p'], $page) ){
    
    $name = $_GET['p']; 
    $currentPage = $page[$name]['file-name'];
    $title = $page[$name]['title'];
        
    require  ROOT_PATH .'controllers/'.$currentPage;    
} 
elseif (isset($_GET['p']) && !empty($_GET['p']) && !array_key_exists($_GET['p'], $page)) {

	require ROOT_PATH .'controllers/errorController.php';
}
else {

    require  ROOT_PATH .'/controllers/indexController.php';
}

$content = ob_get_clean();


if (isset($_GET['p']) && $_GET['p'] === "bilans" ){

	require  ROOT_PATH .'views/template_bilans.php';
}
else{

	require  ROOT_PATH .'views/template.php';
}


