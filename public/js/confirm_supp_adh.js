$(function () {

	var formSupp;


    $('input[name=supprimmer_adh]').click( function(e) {
  	
    	$('#confirm-delete').modal('show');
    	e.preventDefault();
    	formSupp = $(this).parent('form');
    });

    $('#btn-yes').click( function() {

        formSupp[0].submit();
    });

    $('#btn-no').click( function() {

   	    $('#confirm-delete').modal('hide');
   	});
    
});