$(function () {

	var i = 1;

  $('.renouv').click( function(){


  	var form = $(this).parents('tr').find('.renouv-form');

  	if(i%2 == 0){

  		$('.renouv').show('slow');
  		$(this).hide();
  		form.hide('slow');
  		$(this).removeClass('btn-danger');
  		$(this).addClass('btn-success');
  		$(this).val('Renouveler');
  		$(this).show('slow');

  		i++;
  	}
  	else{

  		$('.renouv').hide();
  		$(this).show('slow');
  		form.show('slow');
  		$(this).removeClass('btn-success');
  		$(this).addClass('btn-danger');
  		$(this).val('Annuler');

  		i++;
  	}

  });
    
});