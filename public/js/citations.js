$(function () {

	var citations = [ "La propriété, c'est le vol.  Pierre-Joseph Proudhon",
                    "Nous avons exagéré le superflu, nous n'avons plus le nécessaire.  Pierre-Joseph Proudhon",
                    "L'anarchie, c'est l'ordre sans le pouvoir.  Pierre-Joseph Proudhon",
                    "Il faut trouver une combinaison agricole et industrielle au moyen de laquelle le pouvoir, aujourd’hui dominateur de la société, en devienne l’esclave.   Pierre-Joseph Proudhon",
                    "L'autorité de l'homme sur l'homme est-elle juste ?  Pierre-Joseph Proudhon",
                    "Le barbecue, en gros, c'est un appareil qui te permet de manger des saucisses pratiquement crues mais avec les doigts bien cuits.   Coluche",
                    "Selon les statistiques, il y a une personne sur cinq qui est déséquilibrée. S'il y a 4 personnes autour de toi et qu'elles te semblent normales, c'est pas bon.  JCVD"
                  ];
 var min = Math.ceil(1);
 var max = Math.floor(7);
 var nb = Math.floor(Math.random() * (max - min +1)) + min;

 $('#citation').text(citations[nb-1]);
    
});