<?php

require_once ROOT_PATH .'app/Data_Validation.php';
require_once ROOT_PATH .'app/membersArea.php';
require_once ROOT_PATH .'models/usersModel.php';
require_once ROOT_PATH .'models/adherantsModel.php';
require_once ROOT_PATH .'models/cotisationsModel.php';
require_once ROOT_PATH .'models/fondlutteModel.php';
require_once ROOT_PATH .'models/categoriesModel.php';
require_once ROOT_PATH .'app/Load_Script.php';


if(is_connected()){

	//Au clic sur saisie cotisations button
	if (isset($_POST['saisie_cotisations'])){

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$d =new adherantsModel();
		$users = $d->get_adh_non_virements();
		$alertcoti = "";
		
	    require ROOT_PATH .'views/cotisationsView.php';	
	}
	//si on clic sur recap cotisations
	elseif(isset($_POST['recap_cotisations'])){

		require ROOT_PATH .'helpers/finances/create_coti_tab.php';

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$d =new adherantsModel();
		$users = $d->get_adh_non_virements();
		$year = date('Y');
		$data = create_coti_tab($users,$year);
			
		Load_Script::getInstance()->enqueue_script("js/recap.js");
		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		require ROOT_PATH .'views/recapCotisationsView.php';
	}
	//si on clic sur le détail des coti
	else if(isset($_POST['submit_coti'])){

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$d =new adherantsModel();
		//récup des données nécessaires
		$users = $d->get_adh_non_virements();
		$adh = intval($_POST['adh']);
		$year = intval($_POST['year']);
		$adh_infos = $d->get_adh($adh);

		//si pas d'envoi d'adhérent ou d'année dans le form
		if(empty($adh) || empty($year)){

			$alertcoti = "l'un des champs n'est pas renseigné!"; 

			Load_Script::getInstance()->enqueue_script("js/show_alert.js");

			require ROOT_PATH .'views/cotisationsView.php';
		}
		//sinon on récupère les coti voulues
		else{

			$d =new cotisationsModel();
			//On récupère les cotisations de l'adhérent 
			$cotisations = $d->get_coti_by_adh($adh, $year);
			//on initialise les variables
			$janvier = 0;
			$fevrier = 0;
			$mars = 0;
			$avril = 0;
			$mai = 0;
			$juin = 0;
			$juillet = 0;
			$aout = 0;
			$septembre = 0;
			$octobre = 0;
			$novembre = 0;
			$decembre = 0;
			$alertdetailcoti = "";
			
			// on remplace les valeurs initialisées par les entrées bdd
			foreach($cotisations as $coti){
				if( $coti['mois'] == 0){
					$janvier = $coti['montant'];
				}
				elseif($coti['mois'] == 1){
					$fevrier = $coti['montant'];
				}
				elseif($coti['mois'] == 2){
					$mars = $coti['montant'];
				}
				elseif($coti['mois'] == 3){
					$avril = $coti['montant'];
				}
				elseif($coti['mois'] == 4){
					$mai = $coti['montant'];
				}
				elseif($coti['mois'] == 5){
					$juin = $coti['montant'];
				}
				elseif($coti['mois'] == 6){
					$juillet = $coti['montant'];
				}
				elseif($coti['mois'] == 7){
					$aout = $coti['montant'];
				}
				elseif($coti['mois'] == 8){
					$septembre = $coti['montant'];
				}
				elseif($coti['mois'] == 9){
					$octobre = $coti['montant'];
				}
				elseif($coti['mois'] == 10){
					$novembre = $coti['montant'];
				}
				elseif($coti['mois'] == 11){
					$decembre = $coti['montant'];
				}
				else{
					$alertdetailcoti = "l'adhérant n'a pas cotisé cette année";
				}
			}
		}

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");
		//on appel la vue
		require ROOT_PATH .'views/detailsCotisationsView.php';
	}
	//au clic sur enregistrer
	elseif(isset($_POST['submit_details_coti'])){

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$c =new cotisationsModel();
		$d =new adherantsModel();
		//recup des besoins de la page après traitement
		$users = $d->get_adh_non_virements();
		//Récup de l'adh et de l'année
		$adh = intval($_POST['id_adh']);
		$year = intval($_POST['year']);	
		//Initialisation des valeurs d'entrée utilisateur
		$cotisations_insert_tab = array();
		//récupération des cotisations d'origine
		$cotisations_origin = $c->get_coti_by_adh($adh, $year);
		//init du tableau d'origine
		$cotisation_origin_tab = array(0 => "0", 1 => '0', 2 => "0", 3 => "0", 4 => "0", 5 => "0", 6 => "0", 7 => "0", 8 => "0", 9 => "0", 10 => "0", 11 => "0", );
		//init de l'alert de retour
		$alertcoti ="";

		//Mise au même format des infos

		//remplacement du tableau des saisies utilisateurs
		for ($i=0; $i < 12; $i++) { 

		 	array_push($cotisations_insert_tab, $_POST[$i]);
		}

		//remplacement du tableau d'origine
		if(isset($cotisations_origin) ){
		 	foreach($cotisations_origin as $entree_origine){

		 		if($entree_origine['mois'] === 'O' ){
		 
		 			$cotisation_origin_tab = array_replace($cotisation_origin_tab, array( 0 => $entree_origine['montant']));
		 		}
		 		elseif($entree_origine['mois'] === '1' ){
		 
	 				$cotisation_origin_tab = array_replace($cotisation_origin_tab, array( 1 => $entree_origine['montant']));
		 		}
		 		elseif($entree_origine['mois'] === '2' ){
		 
		 			$cotisation_origin_tab = array_replace($cotisation_origin_tab, array( 2 => $entree_origine['montant']));
		 		}
		 		elseif($entree_origine['mois'] === '3' ){
		 
		 			$cotisation_origin_tab = array_replace($cotisation_origin_tab, array( 3 => $entree_origine['montant']));
		 		}
		 		elseif($entree_origine['mois'] === '4' ){
		 
		 			$cotisation_origin_tab = array_replace($cotisation_origin_tab, array( 4 => $entree_origine['montant']));
		 		}
		 		elseif($entree_origine['mois'] === '5' ){
		 
	 				$cotisation_origin_tab = array_replace($cotisation_origin_tab, array( 5 => $entree_origine['montant']));
		 		}
		 		elseif($entree_origine['mois'] === '6' ){
		 
		 			$cotisation_origin_tab = array_replace($cotisation_origin_tab, array( 6 => $entree_origine['montant']));
		 		}
		 		elseif($entree_origine['mois'] === '7' ){
		 
		 			$cotisation_origin_tab = array_replace($cotisation_origin_tab, array( 7 => $entree_origine['montant']));
		 		}
		 		elseif($entree_origine['mois'] === '8' ){
		 
		 			$cotisation_origin_tab = array_replace($cotisation_origin_tab, array( 8 => $entree_origine['montant']));
		 		}
		 		elseif($entree_origine['mois'] === '9' ){
		 
		 			$cotisation_origin_tab = array_replace($cotisation_origin_tab, array( 9 => $entree_origine['montant']));
		 		}
		 		elseif($entree_origine['mois'] === '10' ){
		 
		 		$cotisation_origin_tab = array_replace($cotisation_origin_tab, array( 10 => $entree_origine['montant']));
		 		}
		 		elseif($entree_origine['mois'] === '11' ){
		 
		 			$cotisation_origin_tab = array_replace($cotisation_origin_tab, array( 11 => $entree_origine['montant']));
		 		}
		 		else{

		 			$alertcoti ="erreur dans le remplacement des cotisations d'origine";
		 		}
		 	}
		}	

		//récupération des différences 
		$diff = array_diff_assoc($cotisations_insert_tab, $cotisation_origin_tab);

		//si il y a des diff
		if(!empty($diff)){
			//pour chaque entrée de $diff
			foreach ($diff as $mois => $value) {
			
				//si l'entrée existe en base de donnée
				if ($c->coti_exist($adh, $year, $mois) === true) {
				
					//update de l'entrée
					$c->update_coti_adh($adh, $year, $mois, $value);
				}	
				//sinon 
				else{

					//insert de l'entrée
					$c->insert_coti_adh($adh, $year, $mois, $value);
				}

				//message de confirm
				$alertcoti = "Cotisation bien Enregistrée.";
			}
		}

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		//appel de la vue
		require ROOT_PATH .'views/cotisationsView.php';	
	}
	//Au clic sur fond de lutte
	elseif(isset($_POST['fond'])){

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$alertFond = "";
		$d =new fondlutteModel();
		$e = new categoriesModel();
		//recup des 20 dernières saisies
		$data = $d->get_fond_infos();
		$solde = $data[0]['solde'];	
		//on appel la vue
		require ROOT_PATH .'views/detail_fond_view.php';
	}
	//au clic sur ajouter transac
	elseif(isset($_POST['new_transac'])){

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		if(isset($_POST['solde']) && is_numeric($_POST['solde'])){

			$solde = $_POST['solde'];
		}
		$d =new categoriesModel();
		$cats = $d->get_category();
		$alertAddTransac = "";

		Load_Script::getInstance()->enqueue_script("js/confirm_add_transac.js");
		
		require ROOT_PATH .'views/add_transac_view.php';
	}
	//à l'ajout d'une transac
	elseif(isset($_POST['bouton_add_transac'])){

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		if(isset($_POST['solde']) && is_numeric($_POST['solde'])){

			$solde = $_POST['solde'];
		}
		$date = $_POST['date_transac'];
		$d = new categoriesModel;
		$cat = $d->get_cat_by_name($_POST['categorie']);
		$catID = $cat[0]['id'];
		if(isset($_POST['type'])){$type = $_POST['type'];}else{$type="non-renseigné";}
		$montant = $_POST['montant'];
		$comm = $_POST['commentaire'];

		if($type === "recette"){

			$newSolde = strval($solde + $montant);
		}
		elseif($type === "depence"){

			$newSolde = strval($solde - $montant);
		}
		else{

			$alertAddTransac = "Le type n'est pas au bon format!";
		}

		$validation = new Data_Validation;

		if(! $validation->verifDate($date)){

			$alertAddTransac = "La date doit être au format date!";
			if(isset($_POST['solde']) && is_numeric($_POST['solde'])){

				$solde = $_POST['solde'];
			}
			$e =new categoriesModel();
			$cats = $e->get_category();

			Load_Script::getInstance()->enqueue_script("js/show_alert.js");
			
			require ROOT_PATH .'views/add_transac_view.php';
		}
		elseif(! $validation->verifNumber($catID)){

			$alertAddTransac = "La catégorie n'est pas au bon format!";
			if(isset($_POST['solde']) && is_numeric($_POST['solde'])){

				$solde = $_POST['solde'];
			}
			$e =new categoriesModel();
			$cats = $e->get_category();

			Load_Script::getInstance()->enqueue_script("js/show_alert.js");
			
			require ROOT_PATH .'views/add_transac_view.php';
		}
		elseif(! $validation->verifText($type)){

			$alertAddTransac = "Le type n'est pas au bon format!";
			if(isset($_POST['solde']) && is_numeric($_POST['solde'])){

				$solde = $_POST['solde'];
			}
			$e =new categoriesModel();
			$cats = $e->get_category();

			Load_Script::getInstance()->enqueue_script("js/show_alert.js");
			
			require ROOT_PATH .'views/add_transac_view.php';
		}
		elseif(! $validation->verifNumber($montant)){

			$alertAddTransac = "Le montant doit être un nombre!";
			if(isset($_POST['solde']) && is_numeric($_POST['solde'])){

				$solde = $_POST['solde'];
			}
			$e =new categoriesModel();
			$cats = $e->get_category();

			Load_Script::getInstance()->enqueue_script("js/show_alert.js");
			
			require ROOT_PATH .'views/add_transac_view.php';
		}
		elseif(! $validation->verifTextNumber($comm)){

			$alertAddTransac = "Le commentaire n'accepte que des chiffres et des lettres!";
			if(isset($_POST['solde']) && is_numeric($_POST['solde'])){

				$solde = $_POST['solde'];
			}
			$e =new categoriesModel();
			$cats = $e->get_category();
			
			require ROOT_PATH .'views/add_transac_view.php';
		}
		else{

			$f = new fondlutteModel();
	    	$validation->cancelSpecialChara( $type );
		    $validation->cancelSpecialChara( $catID );
		    $validation->cancelSpecialChara( $newSolde );
		    $validation->cancelSpecialChara( $comm );
		    $validation->cancelSpecialChara( $montant );
		    $validation->cancelSpecialChara( $date );

		    if($f->add_transac( $type, $catID, $montant, $date, $comm, $newSolde )){

		    	$alertFond ="La transaction a bien été enregistrée!";
		    	$data = $f->get_fond_infos();
		    	$solde = $data[0]['solde'];	
		    	$e = new categoriesModel();
				$cats = $e->get_category();

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");
		    	
		    	require ROOT_PATH .'views/detail_fond_view.php';
		    }
		    else{

		    	$alertAddTransac ="Porblème lors de l'enregistrement";

		    	if(isset($_POST['solde']) && is_numeric($_POST['solde'])){

		    		$solde = $_POST['solde'];
		   		}
		   		$e =new categoriesModel();
		 		$cats = $e->get_category();
		    			
		 		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		   		require ROOT_PATH .'views/add_transac_view.php';
		    } 
		}
	}
	//sinon on appel la page d'acceuil des finances
	else{

		require ROOT_PATH .'views/indexFinancesView.php';
	}
}
else{
	
	require ROOT_PATH .'views/indexView.php';
}

