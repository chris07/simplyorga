<?php


require_once ROOT_PATH .'app/Data_Validation.php';
require_once ROOT_PATH .'app/membersArea.php';
require_once ROOT_PATH .'app/Load_Script.php';
require_once ROOT_PATH .'models/usersModel.php';
require_once ROOT_PATH .'models/categoriesModel.php';


class ParametresController{

	public $data;
	public $vue;
	public $cats;
	public $alertAjoutUser;
	public $alertModifSuppUser;
	public $alertModifSuppCategorie;

	public function __construct($inputs){

		if(is_connected()){

			if( isset( $inputs['ajout_user'] ) ){

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				$this->alertAjoutUser ='';
				$this->vue = ROOT_PATH .'views/ajoutUserView.php';
			}
			elseif( isset( $inputs['new_user'] ) ){

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				require ROOT_PATH .'helpers/parametres/add_user.php';
			}
			elseif( isset( $inputs['liste_users'] ) ){
				
				Load_Script::getInstance()->enqueue_script("js/show_alert.js");
				Load_Script::getInstance()->enqueue_script("js/confirm_supp_user.js");

				$d = new usersModel();
				$this->data = $d->get_users();
				$this->alertModifSuppUser = '' ;
				$this->vue = ROOT_PATH .'views/listeUsersView.php';
			}
			elseif(isset($inputs['modifier']) && isset($inputs['id'])){
			
				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				require ROOT_PATH .'helpers/parametres/update_user.php';	
			}
			elseif(isset($inputs['btn_supp_user']) && isset($inputs['id_delete'])){

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");
				Load_Script::getInstance()->enqueue_script("js/confirm_supp_user.js");

				require ROOT_PATH .'helpers/parametres/delete_user.php';

				$this->vue = ROOT_PATH .'views/listeUsersView.php';
			}
			elseif(isset($inputs['bouton_modif_user']) && !empty($inputs['nom']) && !empty($inputs['prenom']) && !empty($inputs['login'])){

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				require ROOT_PATH .'helpers/parametres/upgrade_user.php';	
			}
			elseif( isset($inputs['liste_categories'])){

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");
				Load_Script::getInstance()->enqueue_script("js/confirm_supp_cat.js");

				$d = new categoriesModel();
				$this->data = $d->get_category();
				$this->cats = get_cats($this->data);
				$this->alertModifSuppCategorie ="";
				$this->vue = ROOT_PATH .'views/listeCategoriesView.php';
			}
			elseif(isset($inputs['ajout_cat'])){

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				$this->alertAjoutCat = '' ;
				$this->vue = ROOT_PATH .'views/ajoutCategorieView.php';
			}
			elseif(isset($inputs['new_cat'])){

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");
				Load_Script::getInstance()->enqueue_script("js/confirm_supp_cat.js");

				require ROOT_PATH .'helpers/parametres/add_cat.php';	
			}
			elseif(isset($inputs['modifier_cat']) && isset($inputs['id'])){

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				require ROOT_PATH .'helpers/parametres/update_cat.php';
			}
			elseif(isset($inputs['btn_supp_cat']) && isset($inputs['id_delete'])){

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				require ROOT_PATH .'helpers/parametres/delete_cat.php';
			}
			elseif(isset($inputs['bouton_modif_cat']) && !empty($inputs['nom']) && !empty($inputs['parent_cat']) ){

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				require ROOT_PATH .'helpers/parametres/upgrade_cat.php';
			}
			else{
				
				$this->vue = ROOT_PATH .'views/indexParametresView.php';
			}
		}
		else{
			
			$this->vue = ROOT_PATH .'views/indexView.php';
		}

	}

}

$controller = new ParametresController($_POST);
require $controller->vue;
