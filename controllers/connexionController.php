<?php


require_once ROOT_PATH .'app/Data_Validation.php';
require_once ROOT_PATH .'app/membersArea.php';
require_once ROOT_PATH .'models/usersModel.php';
require_once ROOT_PATH .'app/Load_Script.php';

if(isset($_POST['send']) && !empty($_POST['login']) && !empty($_POST['password'])){
    
    Load_Script::getInstance()->enqueue_script("js/show_alert.js");
    $validation = new Data_Validation();
    $has_error = false;
    $password = $_POST['password'];
    $login = $_POST['login'];
                
    if( ! $validation->verifTextNumber( $_POST['login'] ) ){

        $validation->cancelSpecialChara( $_POST['login'] );
        $has_error = true;
        $alertConnexion ='Le login ne peut contenir que des chiffres et des lettres';
    } 
    elseif( ! $validation->verifTextNumber( $_POST['password'] ) ){

        $validation->cancelSpecialChara( $_POST['password'] );
        $has_error = true;
        $alertConnexion ='Le mot de passe ne peut contenir que des chiffres et des lettres';
    }
    else{

        $d = new usersModel();
        $id_user = $d->is_connecting($login, $password);

        if($id_user !== false){
                  
            connexion( $id_user );

            $user = $d->get_prenom_user($id_user);

            header('location: index.php?user='.$user);  
        }
        else{


            $alertConnexion ='Login innexistant ou mot de passe incorrect'; 
        }
    }

    require ROOT_PATH .'views/connexionView.php';
}
else{

    $alertConnexion ='';

    require ROOT_PATH .'views/connexionView.php';
}