<?php


require_once ROOT_PATH .'app/Data_Validation.php';
require_once ROOT_PATH .'app/Load_Script.php';
require_once ROOT_PATH .'models/adherantsModel.php';
require_once ROOT_PATH .'app/membersArea.php';


if(is_connected()){

	if (isset($_POST['ajout_adh'])) {

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$alertAjoutAdh = '';

		Load_Script::getInstance()->enqueue_script("js/hide_add_adh.js");
		
		require ROOT_PATH .'views/ajoutAdherantsView.php';
	}
	elseif  (isset($_POST['new_adh'])) {

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		if ( empty($_POST['nom'])
		|| empty($_POST['prenom'])
		|| empty($_POST['adresse'])
		|| empty($_POST['tel'])
		|| empty($_POST['mail'])
		|| empty($_POST['abonnement'])
		|| empty($_POST['cotisation'])) {
			
			$alertAjoutAdh = 'Tous les champs doivent être renseignés.';

			require ROOT_PATH .'views/ajoutAdherantsView.php';
		}
		else{

			$validation = new Data_Validation();
			$nom = $_POST['nom'];
			$prenom = $_POST['prenom'];
			$adresse = $_POST['adresse'];
			$tel = $_POST['tel'];
			$mail = $_POST['mail'];
			if($_POST['date'] === ''){

				$date = null;
				$dateFin = null;
			}
			else{

				$date = $_POST['date'];
				$dateFin = date('Y-m-d',strtotime('+12 month',strtotime($date)));
			}
			$coti = $_POST['coti'];
			if($_POST['abonnement'] === 'oui' ){

				$abonnement = 1;
			}
			else{

				$abonnement = 0;
			}
			if($_POST['cotisation'] === 'oui' ){

				$cotisation = 1;
			}
			else{

				$cotisation = 0;
			}
			if( ! $validation->verifText( $nom ) ){

				$alertAjoutAdh = 'Le nom ne peut contenir que des lettres';
				require ROOT_PATH .'views/ajoutAdherantsView.php';
			}
			elseif( ! $validation->verifText( $prenom ) ){

				$alertAjoutAdh = 'Le prénom ne peut contenir que des lettres';

				require ROOT_PATH .'views/ajoutAdherantsView.php';
			}
			elseif( ! $validation->verifTextNumber( $adresse ) ){

				$alertAjoutAdh = 'L\'adresse ne peut contenir que des chiffres et des lettres';

				require ROOT_PATH .'views/ajoutAdherantsView.php';
			}
			elseif( ! $validation->verifNumber( $tel ) ){

				$alertAjoutAdh = 'Le numéro de téléphone ne peut contenir que des numéros';

				require ROOT_PATH .'views/ajoutAdherantsView.php';
			}
			elseif( ! $validation->verifEmail( $mail ) ){

				$alertAjoutAdh = 'L\'adresse e-mail n\'est pas valide' ;

				require ROOT_PATH .'views/ajoutAdherantsView.php';
			}
			elseif( ! $validation->verifNumber( $abonnement ) ){

				$alertAjoutAdh = 'Erreur de valeur pour l\'abonnement';

				require ROOT_PATH .'views/ajoutAdherantsView.php';
			}
			elseif( $abonnement === 1 && $date === '' ){

				$alertAjoutAdh = 'La date d\'abonnement n\'est pas renseignée';

				require ROOT_PATH .'views/ajoutAdherantsView.php';
			}
			elseif( $abonnement === 1 && $date !== '' && ! $validation->verifDate( $date )  ){

				$alertAjoutAdh = 'La date d\'abonnement n\'est pas valide';

				require ROOT_PATH .'views/ajoutAdherantsView.php';
			}
			elseif(! empty($coti) && ! $validation->verifText( $coti ) ){

				$alertAjoutAdh = 'Le mode de paiement n\'estpas valide';

				require ROOT_PATH .'views/ajoutAdherantsView.php';
			}
			else{

				$d =new adherantsModel();
				$validation->cancelSpecialChara( $nom );
				$validation->cancelSpecialChara( $prenom );
				$validation->cancelSpecialChara( $adresse );
				$validation->cancelSpecialChara( $tel );
				$validation->cancelSpecialChara( $mail );
				$validation->cancelSpecialChara( $date );
				$validation->cancelSpecialChara( $cotisation );
				$validation->cancelSpecialChara( $coti );
				$alertModifSuppAdh ="L'adhérant a bien été enregistré";
				$d->add_adh( $nom, $prenom, $adresse, $tel, $mail, $abonnement, $date, $dateFin,$cotisation, $coti );
				$data_adherants = $d->get_adherants();

				Load_Script::getInstance()->enqueue_script("js/confirm_supp_adh.js");
				Load_Script::getInstance()->enqueue_script("js/search_liste_adh.js");

				require ROOT_PATH .'views/listeAdherantsView.php';
			}
		}	
	}
	elseif(isset($_POST['liste_adh'])){

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$d =new adherantsModel();
		$data_adherants = $d->get_adherants();
		$alertModifSuppAdh ="";

		Load_Script::getInstance()->enqueue_script("js/confirm_supp_adh.js");
		Load_Script::getInstance()->enqueue_script("js/search_liste_adh.js");

		require ROOT_PATH .'views/listeAdherantsView.php';
	}
	elseif(isset($_POST['modifier']) ){

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$id_adh = $_POST['id'];
		settype($id_adh, 'int');

		if(is_int($id_adh)){
			
			$d =new adherantsModel();
			$data_adh = $d->get_adh($id_adh);
			$alertModifAdh ="";

			Load_Script::getInstance()->enqueue_script("js/confirm_modif_adh.js");
			Load_Script::getInstance()->enqueue_script("js/hide_modif_adh.js");

			require ROOT_PATH .'views/updateAdherantView.php';
		}
		else{

			$alertModifSuppAdh = 'Non mais tu t\'est cru où là?';

			Load_Script::getInstance()->enqueue_script("js/confirm_supp_adh.js");
			Load_Script::getInstance()->enqueue_script("js/search_liste_adh.js");

			require ROOT_PATH .'views/listeAdherantsView.php';
		}	
	}
	elseif  (isset($_POST['bouton_modif_adh'])){

			Load_Script::getInstance()->enqueue_script("js/show_alert.js");

			$validation = new Data_Validation();
			$id_adh = $_POST['id'];
			$nom = $_POST['nom'];
			$prenom = $_POST['prenom'];
			$adresse = $_POST['adresse'];
			$tel = $_POST['tel'];
			$mail = $_POST['mail'];
			if($_POST['date'] === ''){

				$date = null;
				$dateFin = null;
			}
			else{

				$date = $_POST['date'];
				$dateFin = date('Y-m-d',strtotime('+12 month',strtotime($date)));
			}

			$coti = $_POST['coti'];
			$d =new adherantsModel();
			$data_adh = $d->get_adh($id_adh);
			if($_POST['abonnement'] === 'oui' ){

				$abonnement = 1;
			}
			else{

				$abonnement = 0;
			}
			if($_POST['cotisation'] === '1' ){

				$cotisation = 1;
			}
			else{

				$cotisation = 0;
			}
			if( ! $validation->verifText( $nom ) ){

				$alertModifAdh = 'Le nom ne peut contenir que des lettres';

				Load_Script::getInstance()->enqueue_script("js/confirm_modif_adh.js");
				Load_Script::getInstance()->enqueue_script("js/hide_modif_adh.js");

				require ROOT_PATH .'views/updateAdherantView.php';
			}
			elseif( ! $validation->verifText( $prenom ) ){

				$alertModifAdh = 'Le prénom ne peut contenir que des lettres';

				Load_Script::getInstance()->enqueue_script("js/confirm_modif_adh.js");
				Load_Script::getInstance()->enqueue_script("js/hide_modif_adh.js");

				require ROOT_PATH .'views/updateAdherantView.php';
			}
			elseif( ! $validation->verifTextNumber( $adresse ) ){

				$alertModifAdh = 'L\'adresse ne peut contenir que des chiffres et des lettres';

				Load_Script::getInstance()->enqueue_script("js/confirm_modif_adh.js");
				Load_Script::getInstance()->enqueue_script("js/hide_modif_adh.js");

				require ROOT_PATH .'views/updateAdherantView.php';
			}
			elseif( ! $validation->verifNumber( $tel ) ){

				$alertModifAdh = 'Le numéro de téléphone ne peut contenir que des numéros';

				Load_Script::getInstance()->enqueue_script("js/confirm_modif_adh.js");
				Load_Script::getInstance()->enqueue_script("js/hide_modif_adh.js");

				require ROOT_PATH .'views/updateAdherantView.php';
			}
			elseif( ! $validation->verifEmail( $mail ) ){

				$alertModifAdh = 'L\'adresse e-mail n\'est pas valide' ;

				Load_Script::getInstance()->enqueue_script("js/confirm_modif_adh.js");
				Load_Script::getInstance()->enqueue_script("js/hide_modif_adh.js");

				require ROOT_PATH .'views/updateAdherantView.php';
			}
			elseif( ! $validation->verifNumber( $abonnement ) ){

				$alertModifAdh = 'Erreur de valeur pour l\'abonnement';

				Load_Script::getInstance()->enqueue_script("js/confirm_modif_adh.js");
				Load_Script::getInstance()->enqueue_script("js/hide_modif_adh.js");

				require ROOT_PATH .'views/updateAdherantView.php';
			}
			elseif( $abonnement === 1 && $date === '' ){

				$alertModifAdh = 'La date d\'abonnement n\'est pas renseignée';

				Load_Script::getInstance()->enqueue_script("js/confirm_modif_adh.js");
				Load_Script::getInstance()->enqueue_script("js/hide_modif_adh.js");

				require ROOT_PATH .'views/updateAdherantsView.php';
			}
			elseif( $abonnement === 1 && $date !== '' && ! $validation->verifDate( $date )  ){

				$alertModifAdh = 'La date d\'abonnement n\'est pas valide';

				Load_Script::getInstance()->enqueue_script("js/confirm_modif_adh.js");
				Load_Script::getInstance()->enqueue_script("js/hide_modif_adh.js");

				require ROOT_PATH .'views/updateAdherantsView.php';
			}
			elseif(! empty($coti) && ! $validation->verifText( $coti ) ){

				$alertModifAdh = 'Le mode de paiement n\'estpas valide';

				Load_Script::getInstance()->enqueue_script("js/confirm_modif_adh.js");
				Load_Script::getInstance()->enqueue_script("js/hide_modif_adh.js");

				require ROOT_PATH .'views/updateAdherantView.php';
			}
			else{

				$d =new adherantsModel();
				$validation->cancelSpecialChara( $nom );
				$validation->cancelSpecialChara( $prenom );
				$validation->cancelSpecialChara( $adresse );
				$validation->cancelSpecialChara( $tel );
				$validation->cancelSpecialChara( $mail );
				$validation->cancelSpecialChara( $date );
				$validation->cancelSpecialChara( $coti );
				$validation->cancelSpecialChara( $cotisation );
				$alertModifSuppAdh ="L'adhérant a bien été Modifié";

				if($d->update_adh( $nom, $prenom, $adresse, $tel, $mail, $abonnement, $date, $dateFin, $cotisation, $coti, $id_adh )){

					$data_adherants = $d->get_adherants();

					Load_Script::getInstance()->enqueue_script("js/confirm_supp_adh.js");
					Load_Script::getInstance()->enqueue_script("js/search_liste_adh.js");

					require ROOT_PATH .'views/listeAdherantsView.php';
				}
				else{

					$alertModifAdh = "Problème lors de l'enregistrement!";

					Load_Script::getInstance()->enqueue_script("js/confirm_modif_adh.js");
					Load_Script::getInstance()->enqueue_script("js/hide_modif_adh.js");

					require ROOT_PATH .'views/updateAdherantView.php';
				}
				
			}
	}
	elseif(isset($_POST['btn_supp_adh'])){

		Load_Script::getInstance()->enqueue_script("js/show_alert.js");

		$id = $_POST['id_delete'];
		settype($id, 'int');

		if(!is_int($id)){

			$alertModifSuppAdh ="Non mais tu t'es cru où là?";

			Load_Script::getInstance()->enqueue_script("js/confirm_supp_adh.js");
			Load_Script::getInstance()->enqueue_script("js/search_liste_adh.js");

			require ROOT_PATH .'views/listeAdherantsView.php';
		}
		else{

			$d =new adherantsModel();
			$d->delete_adh($id);
			$data_adherants = $d->get_adherants();
			$alertModifSuppAdh ="L'adhérant a bien été supprimmé!";

			Load_Script::getInstance()->enqueue_script("js/confirm_supp_adh.js");
			Load_Script::getInstance()->enqueue_script("js/search_liste_adh.js");
			
			require ROOT_PATH .'views/listeAdherantsView.php';
		}
	}
	else{

		require ROOT_PATH .'views/indexAdherantsView.php';
	}
}
else{

	require ROOT_PATH .'views/indexView.php';
}



