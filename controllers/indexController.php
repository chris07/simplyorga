<?php
require_once ROOT_PATH .'app/membersArea.php';
require_once ROOT_PATH .'models/usersModel.php';


//Message de bienvenue
if(!empty($_GET['user'])){

	$validation = new Data_Validation();

	if( $validation->verifText( $_GET['user'] ) ){

		$user = $_GET['user'];
		$bienvenue = 'Bienvenue ' . $user;
		require  ROOT_PATH .'/views/indexView.php';
	}
	else{

		require  ROOT_PATH .'/controllers/errorController.php';
	}
}
else{

	
	$bienvenue = '';

	require  ROOT_PATH .'/views/indexView.php';
}

