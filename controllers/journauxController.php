<?php

require_once ROOT_PATH .'app/Data_Validation.php';
require_once ROOT_PATH .'app/Load_Script.php';
require_once ROOT_PATH .'models/journauxModel.php';
require_once ROOT_PATH .'models/adherantsModel.php';
require_once ROOT_PATH .'app/membersArea.php';






class JournauxController{

	public $alertVentes;
	public $alertbilan;
	public $alertabonn;
	public $data;
	public $vue;
	public $graphs;

	public function __construct($inputs){

		//Si le client est connecté
		if(is_connected()){

			//enregistrement des ventes et objectifs
			if (isset($inputs['ventes'])) {

				Load_Script::getInstance()->enqueue_script("js/switch_objectifs_ventes.js");
				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				$this->alertVentes = "";
				$this->vue = ROOT_PATH .'views/ventesJournauxView.php';
			}
			//si on enregistre une vente ou un objectif
			elseif(isset($inputs['send_vente_journal'])){

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				$this->alertVentes = "";

				include ROOT_PATH .'helpers/journaux/register_vente.php';

				$this->vue = ROOT_PATH .'views/ventesJournauxView.php';

			}
			//bilans et graphiques
			elseif (isset($inputs['bilans'])) {

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				$this->alertbilan = "";
				$this->vue = ROOT_PATH .'views/bilansJournauxView.php';	
			}
			//affichage graphique
			elseif(isset($inputs['submit']) ){

				require ROOT_PATH .'helpers/journaux/create_graph.php';

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				$this->alertbilan = '' ;

				include ROOT_PATH .'helpers/journaux/affichage_graphs.php';

				$this->graphs = $graphs;
				$this->vue = ROOT_PATH .'views/bilansJournauxView.php';
			}
			//renouvellement des abonnements
			elseif (isset($inputs['abonnements'])) {

				require ROOT_PATH .'helpers/journaux/create_renouv_liste.php';

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");
				Load_Script::getInstance()->enqueue_script("js/show_renouv.js");

				$d =new adherantsModel();
				$this->alertabonn = "";
				$adhs = $d->get_adherants();
				$this->data = create_renouv_list($adhs);
				$this->vue = ROOT_PATH .'views/abonnementsJournauxView.php';
			}
			//renouvellement d'un abonnement
			elseif (isset($inputs['modif_date']) ) {

				require ROOT_PATH .'helpers/journaux/create_renouv_liste.php';

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				include ROOT_PATH .'helpers/journaux/register_abonn.php';

				$c =new adherantsModel();
				$adhs = $c->get_adherants();
				$this->data = create_renouv_list($adhs);
				$this->vue = ROOT_PATH .'views/abonnementsJournauxView.php';
			}
			//Affichage de l'index des journaux (par default)
			else{

				Load_Script::getInstance()->enqueue_script("js/show_alert.js");

				$this->alertVentes = "";
				$this->vue = ROOT_PATH .'views/indexJournauxView.php';
			}
		}
		//Sinon Affichage index du site
		else{
			
			$this->vue = ROOT_PATH .'views/indexView.php';
		}
	}

}

$controller = new JournauxController($_POST);
require $controller->vue;


